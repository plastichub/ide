define([
    'dcl/dcl',
    'dojo/_base/lang',
    'xide/utils',
    'xide/types',
    'xide/factory',
    'xide/widgets/WidgetBase',
    "xcf/views/DeviceTreeView",
    "xide/views/_PanelDialog"
], function (dcl,lang, utils, types,factory, WidgetBase,DeviceTreeView,_PanelDialog) {
    /**
     * Url generator for device/driver/[command|block|variable]
     *
     * @param device
     * @param driver
     * @param block
     * @param prefix
     * @returns {*}
     */
    function toUrl(device, driver, block, prefix) {
        prefix = prefix || '';
        const pattern = prefix + "deviceScope={deviceScope}&device={deviceId}&driver={driverId}&driverScope={driverScope}&block={block}";
        return lang.replace(
            pattern,
            {
                deviceId: device.id,
                deviceScope: device.scope,
                driverId: driver.id,
                driverScope: driver.scope,
                block: block.id
            });
    }
    /**
     * Filter function to reject selections in a device - tree - view
     * @param item
     * @param acceptCommand
     * @param acceptVariable
     * @param acceptDevice
     * @param acceptGroup
     * @returns {*}
     */
    function accept(item, acceptCommand, acceptVariable, acceptDevice, acceptGroup) {
        const reference = item.ref || {};
        const block = reference.item;
        const device = reference.device;
        const driver = reference.driver;
        const scope = block ? block.getScope() : null;
        const isGroup = item.isDir;
        const isDevice = item.isDevice;
        let isCommand = block ? block.isCommand || block.declaredClass.indexOf('model.Command') !== -1 : false;
        const isVariable = block ? block.declaredClass.indexOf('model.Variable') !== -1 : false;

        if(block && block.isCommand === true){
            isCommand = true;
        }
        if (isCommand && acceptCommand) {
            return toUrl(device, driver, block, 'command://');
        }
        if (isVariable && acceptVariable) {
            return toUrl(device, driver, block, 'variable://');
        }
        if (isGroup && acceptGroup) {
            return item;
        }
        if (isDevice && acceptDevice) {
            return item;
        }

        return false;
    }
    /**
     *
     * @param parent
     * @param ctx
     * @param driver
     * @param device
     * @param type
     * @returns {*}
     */
    function createDeviceView(parent,ctx,driver,device,type) {
        const ACTION = types.ACTION;
        const deviceScope = device ? device.info.deviceScope : 'user_devices';

        const permissions = [
            ACTION.EDIT,
            ACTION.RELOAD,
            ACTION.DELETE,
            ACTION.SELECTION,
            ACTION.TOOLBAR,
            ACTION.SEARCH
        ];

        if(type==='group'){
            permissions.remove(ACTION.EDIT);
            permissions.push('File/New Group');
        }


        const grid = utils.addWidget(DeviceTreeView, {
            title: 'Devices',
            collection: ctx.getDeviceManager().getStore(deviceScope),
            delegate: ctx.getDeviceManager(),
            blockManager: ctx.getBlockManager(),
            ctx: ctx,
            icon: 'fa-sliders',
            showHeader: false,
            scope: deviceScope,
            resizeToParent: true,
            permissions:permissions
        }, null, parent, true);

        grid._on('selectionChanged',function(evt){
            const selection = evt.selection, item = selection && selection.length==1 ? selection[0] : null;

            if(!item){
                return;
            }
            const isAccepted = accept(item,type==='command',type==='variable',type==='device',type==='group');
            if(isAccepted){
                grid._selected = isAccepted;
                grid._selection = selection;
            }
        });

        setTimeout(function(){
            grid.showToolbar(true);
            grid.resize();
        },500);
        return grid;
    }
    const Module = dcl(WidgetBase, {
        declaredClass: "xcf.widgets.CommandPickerWidget",
        minHeight: "400px;",
        value: "",
        options: null,
        title: 'Command',
        templateString: "<div class='widgetContainer widgetBorder widgetTable' style=''>" +
        "<table border='0' cellpadding='5px' width='100%' >" +
        "<tbody>" +
        "<tr attachTo='extensionRoot'>" +
            "<td width='20%' class='widgetTitle'><span>${!title}<span></td>" +
            "<td class='widgetValue2' valign='middle' attachTo='valueNode'></td>" +
            "<td class='extension' width='25px' attachTo='button0'></td>" +
            "<td class='extension' width='25px' attachTo='button1'></td>" +
            "<td class='extension' width='25px' attachTo='button2'></td>" +
        "</tr>" +
        "</tbody>" +
        "</table>" +
        "<div attachTo='expander' onclick='' style='width:100%;'></div>" +
        "<div attachTo='last'></div>" +
        "</div>",
        onValueChanged:function(value,gridView){
            value = value.newValue || value;
            this.setValue(value);
            const ci = this.userData;
            const block = ci.block;
            let blockScope = block ? block.scope : null;

            if(!gridView){
                return;
            }

            const selection = gridView._selection || [];
            if(selection[0] && selection[0].ref && selection[0].ref.item){
                blockScope = selection[0].ref.item.scope;
            }
            const title = blockScope ? blockScope.toFriendlyName && blockScope.toFriendlyName(block, value) : value;
            value = _.isString(value) ? value : value.name;//grab the device's name
            this.commandWidget.set('value',value,title);
        },
        onSelect: function (ctx,driver,device) {
            const thiz = this;
            const _defaultOptions = {}, ci = this.userData;
            utils.mixin(_defaultOptions, this.options);
            const value = utils.toString(this.userData['value']);
            let title = ci.pickerType === "command" ? "Select Command" : "Select Variable";
            if(ci.pickerType==='group'){
                title = 'Select Group';
            }
            if(ci.pickerType==='device'){
                title = 'Select Device';
            }
            const dlg = new _PanelDialog({
                size: types.DIALOG_SIZE.SIZE_NORMAL,
                bodyCSS: {
                    'height': 'auto',
                    'min-height': '500px',
                    'padding': '8px',
                    'margin-right': '16px'
                },
                picker: null,
                title:title,
                onOk: function () {
                    const selected = thiz.picker._selected;
                    selected && thiz.onValueChanged(selected,thiz.picker);
                },
                onShow:function(panel,contentNode,instance){
                    //var thiz = dlg.owner;
                    const picker = createDeviceView(contentNode,ctx,driver,device,ci.pickerType);
                    thiz.picker = picker;
                    thiz.add(picker, null, false);
                    return $(picker.domNode);
                }
            });
            return dlg.show();
        },
        postMixInProperties: function () {
            this.inherited(arguments);
            if (this.userData && this.userData.title) {
                this.title = this.userData.title;
            }
            if ((this.userData && this.userData.vertical === true) || this.vertical === true) {
                this.templateString = "<div class='widgetContainer widgetBorder widgetTable' style=''>" +
                    "<table border='0' cellpadding='5px' width='100%' >" +
                    "<tbody>" +
                    "<tr attachTo='extensionRoot'>" +
                    "<td width='100%' class='widgetTitle'><span><b>${!title}</b><span></td>" +
                    "</tr>" +
                    "<tr attachTo='extensionRoot'>" +
                    "<td width='50%' class='widgetValue2' valign='middle' attachTo='valueNode'></td>" +
                    "<td class='extension' width='25px' attachTo='button0'></td>" +
                    "<td class='extension' width='25px' attachTo='button1'></td>" +
                    "<td class='extension' width='25px' attachTo='button2'></td>" +
                    "</tr>" +
                    "</tbody>" +
                    "</table>" +
                    "<div attachTo='expander' onclick='' style='width:100%;'></div>" +
                    "<div attachTo='last'></div>" +
                    "</div>"
            }
        },
        fillTemplate: function () {

            const thiz = this;
            const value = utils.toString(this.userData['value']) || '';
            const ci = this.userData, block = ci.block || {}, blockScope = block.scope || {}, ctx = blockScope && blockScope.ctx ? blockScope.ctx : this.ctx, pickerType = ci.pickerType, driver = blockScope.driver, device = blockScope.device, deviceManager = ctx.getDeviceManager(), driverManager = ctx.getDriverManager();

            //add the Select Widget for local commands
            const CIS = [], isCommand = pickerType == 'command', isGroup = pickerType == 'group', isDevice = pickerType == 'device';

            let title = 'Command';

            if(value.indexOf('://')!==-1 && blockScope.toFriendlyName){
                title = blockScope.toFriendlyName(block,value);
            }
            if(isDevice) {
                CIS.push(utils.createCI('value', 13, 'Select Device', {
                    group: 'General',
                    value: value,
                    title: " "
                }));
            }else if(isGroup){
                CIS.push(utils.createCI('value', 13, 'Select Group', {
                    group: 'General',
                    value: value,
                    title: " "
                }));
            }else {
                if (blockScope.blockStore) {
                    CIS.push(utils.createCI('value', 3, 'Volume', {
                        group: 'General',
                        title: title,
                        value: value,
                        widget: {
                            store: blockScope.blockStore,
                            labelField: 'name',
                            valueField: 'id',
                            search: true,
                            title: title,
                            query: this.query ? this.query : {
                                group: isCommand ? 'basic' : 'basicVariables'
                            },
                            templateString: '<select disabled="${!disabled}" value="${!value}" title="${!title}" data-live-search="${!search}"  data-style="${!style}" class="selectpicker" attachTo="selectNode">'
                        }
                    }));
                } else {
                    CIS.push(utils.createCI('value', 13, 'Driver Variable', {
                        group: 'General',
                        value: value,
                        title: " "
                    }));
                }
            }
            factory.renderCIS(CIS, this.valueNode, this).then(function (widgets) {
                const commandWidget = widgets[0];
                const btn = factory.createSimpleButton('', 'fa-magic', 'btn-default', {
                    style: ''
                });
                thiz.commandWidget = commandWidget;
                commandWidget._on('onValueChanged',function(e){
                    thiz.onValueChanged(e);
                });
                $(btn).click(function () {
                    thiz.onSelect(ctx,driver,device);
                });
                $(thiz.button0).append(btn);
                thiz.add(commandWidget,null,false);
            });
        },
        startup: function () {
            try {
                this.fillTemplate();
                this.onReady();
            } catch (e) {
                logError(e);
            }
        }
    });
    dcl.chainAfter(Module,"destroy");
    return Module;
});
