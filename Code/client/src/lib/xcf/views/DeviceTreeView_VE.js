define([
    'xdojo/declare',
    'xdojo/has',
    'dojo/_base/lang',
    'xide/types',
    'xide/utils',
    "dijit/focus",
    "davinci/ui/dnd/DragManager",
    "davinci/ve/utils/GeomUtils",
    "davinci/ui/dnd/DragSource",
    "davinci/ve/metadata",
    "davinci/ve/tools/CreateTool"

], function (declare, has, lang, types, 
    utils, FocusUtils, DragManager, GeomUtils, DragSource, Metadata, CreateTool) {
    const ACTION = types.ACTION;
    const ACTION_TYPE = types.ACTION;
    const ACTION_ICON = types.ACTION_ICON;
    const ctx = window.sctx;
    const test = false;
    let marantz;
    let marantzInstance;

    const isVE = has('xideve');

    const _dragStart = function (from, node, context, e, proto, userData, properties) {
        proto = lang.clone(proto);
        lang.mixin(proto.properties, properties);
        const data = e.dragSource.data;
        userData = data.userData || userData;
        userData.onCreate = function () {

        }
        Metadata.getHelper(proto.type, 'tool').then(function (ToolCtor) {
            // Copy the data in case something modifies it downstream -- what types can data.data be?
            const tool = new(ToolCtor || CreateTool)(dojo.clone(proto), userData);
            context.setActiveTool(tool);
        }.bind(this));

        // Sometimes blockChange doesn't get cleared, force a clear upon starting a widget drag operation
        context.blockChange(false);
        // Place an extra DIV onto end of dragCloneDiv to allow
        // posting a list of possible parent widgets for the new widget
        // and register the dragClongDiv with Context
        if (e._dragClone) {
            $(e._dragClone).addClass('paletteDragContainer');
            //domClass.add(e._dragClone, 'paletteDragContainer');
            dojo.create('div', {
                className: 'maqCandidateParents'
            }, e._dragClone);
        }
        //FIXME: Attach dragClone and event listeners to tool instead of context?
        context.setActiveDragDiv(e._dragClone);
    };
    const _dragEnd = function () {
        if (FocusUtils.curNode && FocusUtils.curNode.blur) {
            FocusUtils.curNode.blur();
        }
    };
    function createProperties(item, url) {
        if (!item.ref) {
            return {};
        }
        const block = item.ref.item;
        const props = {
            block: url,
            script: '' + block.name,
            targetevent: 'click'
        };
        return props;
    }

    function createPropertiesVariable(item, url) {
        if (!item.ref) {
            return {};
        }
        const block = item.ref.item;
        const props = {
            block: url,
            script: '' + block.name,
            targetevent: '',
            sourceevent: 'onDriverVariableChanged',
            targetproperty: 'label',
            sourceeventnamepath: 'item.name',
            sourceeventvaluepath: 'item.value',
            mode: 1
        };
        return props;
    }
    const makeDNDG = function (from, node, context, proto, userData, properties) {

        if (!context) {
            return;
        }
        const clone = node.domNode;

        const ds = new DragSource(node.domNode, "component", node, clone);

        ds.targetShouldShowCaret = true;
        ds.returnCloneOnFailure = false;
        const result = {
            handles: [],
            context: context,
            node: clone,
            ds: ds,
            destroy: function () {
                delete this.context;
                _.each(this.handles, dojo.disconnect);
                delete this.handles;
                delete this.node.__vednd;
                delete this.node;
                delete this.ds;
            }
        };
        const handles = result.handles;
        /**
         * outer handlers:
         */
        handles.push(dojo.connect(ds, "onDragStart", dojo.hitch(from, function (e) {
            _dragStart(from, node, context, e, proto, userData, properties);
        }))); // move start

        handles.push(dojo.connect(ds, "onDragEnd", dojo.hitch(from, function (e) {
            _dragEnd();
        }))); // move end
        /**
         * now the inner handlers
         */
        handles.push(dojo.connect(node.domNode, "onmouseover", function (e) {
            node._mouseover = true;
        }));

        handles.push(dojo.connect(node.domNode, "onmouseout", function (e) {
            node._mouseover = false;
        }));

        handles.push(dojo.connect(node.domNode, "onmousedown", function (e) {
            const DragManager = from.dragMananager;
            DragManager.document = context.getDocument();
            const frameNode = context.frameNode;
            if (frameNode) {
                const coords = dojo.coords(frameNode);
                const containerNode = context.getContainerNode();
                DragManager.documentX = coords.x - GeomUtils.getScrollLeft(containerNode);
                DragManager.documentY = coords.y - GeomUtils.getScrollTop(containerNode);
            }
        }));

        return result;
    };
    /**
     * Url generator for device/driver/[command|block|variable]
     *
     * @param device
     * @param driver
     * @param block
     * @param prefix
     * @returns {*}
     */
    function toUrl(device, driver, block, prefix) {
        prefix = prefix || '';
        const pattern = prefix + "deviceName={deviceName}&deviceScope={deviceScope}&device={deviceId}&driver={driverId}&driverScope={driverScope}&block={block}";
        const url = lang.replace(
            pattern, {
                deviceId: device.id,
                deviceScope: device.scope,
                driverId: driver.id,
                driverScope: driver.scope,
                block: block.id,
                deviceName:device.name
                
            });
        return url;
    }
    /**
     * Filter function to reject selections in a device - tree - view
     * @param item
     * @param acceptCommand
     * @param acceptVariable
     * @param acceptDevice
     * @returns {*}
     */
    function accept(item, acceptCommand, acceptVariable, acceptDevice) {
        const reference = item.ref || {};
        const block = reference.item;
        const device = reference.device;
        const driver = reference.driver;
        const scope = block ? block.getScope() : null;
        const isCommand = block ? block.isCommand || block.declaredClass.indexOf('model.Command') !== -1 : false;
        const isVariable = block ? block.declaredClass.indexOf('model.Variable') !== -1 : false;

        if (isCommand && acceptCommand) {
            return toUrl(device, driver, block, 'command://');
        }
        if (isVariable && acceptVariable) {
            return toUrl(device, driver, block, 'variable://');
        }
        return false;
    }
    const _protoDefault = {
        children: "",
        name: "RunScript",
        properties: {
            style: "position:relative",
            scopeid: "",
            targetevent: "click"
        },
        type: "xblox/RunScript",
        userData: {}
    };
    const Module = declare('xcf.views.DeviceTreeView', null, {
        dragMananager: null,
        veContext: null,
        onAppReady: function (evt) {
            if (this.editorContext && this.editorContext != evt.context) {
                this.editorContext = evt.context;
            }
            if (!this.dragMananager) {
                this.dragMananager = DragManager;
            }
            this.editorContext = evt.context;
            this.updateDragManager(evt.context);
        },
        onDragStart: function (e) {},
        onDragEnd: function (e) {},
        isDnd: function (item) {
            return item.isDir != true && item.isDnd !== false;
        },
        onDeviceStateChanged: function (e) {
            this.grid.refresh();
        },
        updateDragManager: function (context) {
            this.dragMananager.document = context.getDocument();
            const frameNode = context.frameNode;
            if (frameNode) {
                const coords = dojo.coords(frameNode);
                const containerNode = context.getContainerNode();
                this.dragMananager.documentX = coords.x - GeomUtils.getScrollLeft(containerNode);
                this.dragMananager.documentY = coords.y - GeomUtils.getScrollTop(containerNode);
            }
        },
        onContextDestroyed: function (context) {
            if (context.__veDNDDevHandles) {
                _.each(context.__veDNDDevHandles, utils.destroy);
                context.__veDNDDevHandles;
            }
            delete this.editorContext;
        },
        startup: function () {
            this.subscribe([
                types.EVENTS.ON_APP_READY,
                types.EVENTS.ON_DEVICE_STATE_CHANGED,
                types.EVENTS.ON_CONTEXT_DESTROYED
            ]);
            const thiz = this;
            const ctx = thiz.ctx;
            const deviceManager = ctx.getDeviceManager();
            const driverManager = ctx.getDriverManager();

            this._on('selectionChanged', function (evt) {
                const selection = evt.selection;
                const item = selection && selection.length == 1 ? selection[0] : null;

                if (!item || !thiz.editorContext) {
                    return;
                }

                const type = 'command';
                const reference = item.ref || {};
                const block = reference.item;
                const device = reference.device;
                const driver = reference.driver;
                const scope = block ? block.getScope() : null;
                const isCommand = block ? block.isCommand || block.declaredClass.indexOf('model.Command') !== -1 : false;
                const isVariable = block ? block.declaredClass.indexOf('model.Variable') !== -1 : false;

                const isAccepted = accept(item, isCommand, isVariable, type == 'command');
                if (!isAccepted) {
                    return;
                }
                const row = thiz.row(item);
                const node = row.element;

                if (row.element.__vednd) {
                    return;
                }
                node.__vednd = true;

                const prefix = isCommand ? 'command://' : 'variable://';
                const url = toUrl(device, driver, block, prefix);

                const _props = isCommand ? createProperties(item, url) : createPropertiesVariable(item, url);
                const _editorContext = thiz.editorContext;
                const rowObject = {
                    domNode: node
                };
                if (_editorContext) {
                    if (!_editorContext.__veDNDDevHandles) {
                        _editorContext.__veDNDDevHandles = [];
                    }
                    if (this.isDnd(item)) {
                        _editorContext.__veDNDDevHandles.push(makeDNDG(this, rowObject, _editorContext, _protoDefault, item, _props));
                    }
                }
            });
            return this.inherited(arguments);
        }
    });
    return Module;
});