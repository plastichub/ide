/** @module xgrid/Base **/
define([
    "dcl/dcl",
    'xide/types',
    'xide/utils',
    "dojo/string",
    "xide/_base/_Widget",
    'xaction/ActionProvider',
    'xaction/Toolbar'
], function (dcl, types,
             utils, string, _XWidget, ActionProvider,Toolbar) {

    const Module = dcl([_XWidget,ActionProvider.dcl,Toolbar.dcl],{
            declaredClass:'xcf.views.GlobalConsole',
            templateString:'<div attachTo="template" class="grid-template widget" style="width: 100%;height: 100%;overflow: hidden !important;position: relative;padding: 0px;margin: 0px">' +
                '<div attachTo="header" class="view-header row bg-opaque" style=";width:100%;height: 30px;min-height: 30px;"></div>' +
                '<div attachTo="logNode" style="overflow: hidden;width:inherit">' +
                    '<div class="logView" style="display:inline-block;width:100%;overflow: auto;height:inherit" attachTo="logView">'+

                    '</div>'+
                '</div>'+
            '</div>',
            defaultPanelOptions:{
                w: '100%',
                title:'Output'
            },
            defaultPanelType:'DefaultFixed',
            resizeToParent:true,
            toolbarArgs:{
                attachToGlobal:true
            },
            permissions : [
                'File/Stop',
                'File/Delete',
                'File/Start'
            ],
            stopped:false,
            hasFlag:function(deviceInfo,output){
                const ctx = this.ctx, deviceManager = ctx.getDeviceManager(), device = deviceManager.getDeviceStoreItem(deviceInfo);

                if(!device){
                    return;
                }

                deviceInfo = deviceManager.toDeviceControlInfo(device);

                if(!deviceInfo){
                    return;
                }
                const LOGGING_FLAGS = types.LOGGING_FLAGS;
                const OUTPUT = types.LOG_OUTPUT;
                let flags = deviceInfo.loggingFlags;
                flags = _.isString(flags) ? utils.fromJson(flags) : flags || {};

                const flag = flags[output];

                if(flag ==null){
                    return false;
                }

                if(!(flag & LOGGING_FLAGS.GLOBAL_CONSOLE)) {
                    return false;
                }

                return true;
            },
            resize:function(){
                const parent = this.getParent();

                const thiz = this;
                const toolbar = this.getToolbar();
                let noToolbar = false;
                const topOffset = 0;
                const aceNode = $(this.aceNode);


                if(!toolbar ||  (toolbar && toolbar.isEmpty())){
                    noToolbar = true;
                }else{
                    if(toolbar) {
                        toolbar.resize();
                    }
                }

                const totalHeight = $(thiz.domNode).height();
                const topHeight = noToolbar==true ? 0 : $(thiz.header).height();
                const footerHeight = 0;
                let finalHeight = totalHeight - topHeight - footerHeight;

                if(toolbar){
                    finalHeight-=4;
                }
                if (finalHeight > 50) {
                    $(this.logNode).height(finalHeight + 'px');
                }
            },
            runAction: function (action) {
                action = this.getAction(action);
                if (!action) {
                    return false;
                }

                const self = this, command = action.command, ACTION = types.ACTION, result = false;

                if(command === ACTION.DELETE){
                    $(this.logView).empty();
                }

                if(command === 'File/Stop'){
                    this.stopped=true;
                }
                if(command === 'File/Start'){
                    this.stopped=false;
                }
            },
            getSelection:function(){
              return null;
            },
            getLoggingActions: function (permissions) {
                const actions = [], self = this, ACTION = types.ACTION, ICON = types.ACTION_ICON;

                actions.push(this.createAction({
                    label: 'Delete',
                    command: 'File/Delete',
                    icon: 'fa-remove',
                    tab: 'Home',
                    group: 'Organize',
                    mixin:{
                        quick:true
                    }
                }));
                actions.push(this.createAction({
                    label: 'Stop',
                    command: 'File/Stop',
                    icon: 'fa-stop',
                    tab: 'Home',
                    group: 'Organize',
                    mixin:{
                        quick:true
                    }
                }));
                actions.push(this.createAction({
                    label: 'Stop',
                    command: 'File/Start',
                    icon: 'fa-play',
                    tab: 'Home',
                    group: 'Organize',
                    mixin:{
                        quick:true
                    }
                }));
                return actions;
            },
            createWidgets:function(bottom,top){                
            },
            getConsole:function(){
                return this.console;
            },
            printTemplate:'<span style="display:block;margin-bottom: 6px" class="widget border-top-dark"># ${time} - ${command}<br/>\t ${result}</span>',
            printCommand:function(command,result,template,addTime){
                const where = this.logView;
                const time = addTime!==false ? moment().format("HH:mm:SSS") : "";

                const content = utils.substituteString(template || this.printTemplate,{
                    command:command,
                    result:result,
                    time:time
                });
                const node = $(content);
                where.appendChild(node[0]);
                node[0].scrollIntoViewIfNeeded();

                return node;
            },
            log: function (msg,split,markHex) {

                if(this.stopped){
                    return;
                }

                const printTemplate = '<pre style="font-size:100%;padding: 0px;" class="">    ${time} - ${result}</pre>';
                let out = '';
                

                if (_.isString(msg)) {
                    if(split!==false) {
                        out += msg.replace(/\n/g, '<br/>');
                    }else{
                        out = "" + msg;
                        if(markHex) {
                            out = utils.markHex(out, '<span class="">','</span>');
                        }
                    }
                } else if (_.isObject(msg) || _.isArray(msg)) {
                    out += JSON.stringify(msg, null, true);
                }

                const items = [out];
                for (let i = 0; i < items.length; i++) {
                    this.printCommand(items[i],'',this.logTemplate,true);
                }
            },
            getDeviceString:function(device,deviceInfo,suffix){

                let out = (device.scope ==='user_devices' ? "User Device - " : "System Device - ");
                out+= "<b>" + device.name +"</b>" + " - " + deviceInfo.host + ':'+deviceInfo.port+'@'+deviceInfo.protocol + '<span class="text-default"><b>' +(suffix || "") +'</b></span>';
                return out;
            },
            print:function(){
            },
            _lastDeviceString:'',
            onDeviceMessageExt:function(evt){


                const messages= evt.messages.length ? evt.messages : [evt.raw];
                const self = this, device = evt.device, deviceInfo = evt.deviceInfo, OUTPUT = types.LOG_OUTPUT;

                if(!this.hasFlag(deviceInfo,OUTPUT.RESPONSE)){
                    return;
                }
                const deviceString = this.getDeviceString(device,deviceInfo," Message : ");
                const sameDevice = false;
                _.each(messages,function (message) {
                    if(message.length) {
                        const string = '<span class="text-success">' + deviceString + ' ' + message;
                        self.log(string, false, true);
                    }
                })
            },
            onDeviceCommand:function(evt){
                const LOGGING_FLAGS = types.LOGGING_FLAGS, OUTPUT = types.LOG_OUTPUT;

                if(evt.__d2){
                    return;
                }

                evt.__d2 = true;

                const self = this;
                const device = evt.device;
                const deviceInfo = evt.deviceInfo;
                let flags = deviceInfo.loggingFlags;
                flags = _.isString(flags) ? utils.fromJson(flags) : flags || {};

                if(!this.hasFlag(deviceInfo,OUTPUT.SEND_COMMAND)){
                    return;
                }

                const deviceString = this.getDeviceString(device,deviceInfo," Command: ") + "<span class='text-default'>\"" + evt.name + "\"</span> ";
                const string = '<span class="text-info">' + deviceString  + '</span><span class="text-warning">'+evt.command.trim()+'</span>';

                self.log(string,false,false);
                //"User Device - <b>Marantz</b> - 192.168.1.20:23@tcp<span class="text-default"><b> Command: </b></span><span class='text-default'>"Ping - Power"</span> "
                //"<span class="text-info">User Device - <b>Marantz</b> - 192.168.1.20:23@tcp<span class="text-default"><b> Command: </b></span><span class='text-default'>"Ping - Power"</span> </span><span><span class="text-warning">PW?
                //</span>"
                //"<span class="text-info">User Device - <b>Marantz</b> - 192.168.1.20:23@tcp<span class="text-default"><b> Command: </b></span><span class='text-default'>"Ping - Power"</span> </span><span><span class="text-warning">PW?
                //</span>"
                //"<span class="text-info">User Device - <b>Marantz</b> - 192.168.1.20:23@tcp<span class="text-default"><b> Command: </b></span><span class='text-default'>"Ping - Power"</span> </span><span class="text-warning"></span>"
            },
            extract:function(evt){

                const ctx = this.ctx, deviceManager = ctx.getDeviceManager();

                const device = evt.device && evt.device.info ? evt.device : deviceManager.getDeviceStoreItem(evt.device);
                const deviceInfo = device && device.info ? device.info : deviceManager.toDeviceControlInfo(device);
                return {
                    device:device,
                    info:deviceInfo
                }
            },
            onDeviceConnected:function(evt){

                const data = this.extract(evt), OUTPUT = types.LOG_OUTPUT;


                if(evt.__d2){
                    return;
                }
                evt.__d2 = true;
                
                if(!data.device){
                    return false;
                }
                if(evt.isReplay===true && data.device.state ===types.DEVICE_STATE.READY){
                    return false;
                }

                if(!this.hasFlag(data.info,OUTPUT.DEVICE_CONNECTED)){
                    return;
                }

                const deviceString = this.getDeviceString(data.device,data.info," has been connected");
                const string = '<span class="text-info">' + deviceString  + '</span>';
                this.log(string,false,true);

            },
            onDeviceDisconnected:function(evt){
                const data = this.extract(evt), OUTPUT = types.LOG_OUTPUT;


                if(evt.__d2){
                    return;
                }

                evt.__d2 = true;

                if(!data.device){
                    return false;
                }
                if(!this.hasFlag(data.info,OUTPUT.DEVICE_DISCONNECTED)){
                    return;
                }
                const deviceString = this.getDeviceString(data.device,data.info," has been disconnected");
                const string = '<span class="text-warning">' + deviceString  + '</span></span>';
                this.log(string,false,true);
            },
            startup:function(){

                if (this.permissions) {
                    let _defaultActions = [];//DefaultActions.getDefaultActions(this.permissions, this, this);
                    _defaultActions = _defaultActions.concat(this.getLoggingActions(this.permissions));
                    this.addActions(_defaultActions);
                }
                this.showToolbar(true);

                this.getToolbar().setActionEmitter(this);

                this.subscribe(types.EVENTS.ON_DEVICE_MESSAGE_EXT);
                this.subscribe(types.EVENTS.ON_DEVICE_COMMAND);
                this.subscribe(types.EVENTS.ON_DEVICE_CONNECTED);
                this.subscribe(types.EVENTS.ON_DEVICE_DISCONNECTED);                
                this.showToolbar(true);
                utils.resizeTo(this.getToolbar(),this.header,true,false);
                this.getToolbar().resize();


                //var toolbar = this.getToolbar();

                /*
                toolbar.$navBar.append('<input name="search" placeholder="Start typing here" class="searchInput" type="text">');

                var input = toolbar.$navBar.find('.searchInput');
                this.searchInput = $(input);
                $(input).hideseek({
                    list: this.$logView,
                    highlight:true
                });*/

            }
        });

    return Module;

});