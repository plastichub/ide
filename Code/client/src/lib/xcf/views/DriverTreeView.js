/** @module xcf/views/DriverTreeView */
define([
    'xdojo/declare',
    'xide/utils',
    'xide/types',
    'xide/factory',
    'xcf/views/ProtocolTreeView',
    'xgrid/Grid',
    'xgrid/TreeRenderer',
    'xgrid/Search',
    "xide/views/_ActionMixin",
    'dstore/SimpleQuery',
    'dstore/Filter',
    'xaction/DefaultActions',
    "xide/views/_Dialog",
    "xide/views/_Panel",
    "xide/wizard/WizardBase",
    "dojo/Deferred",
    'dojo/promise/all',
    "xide/widgets/_Widget"
], function (declare, utils, types,
    factory, ProtocolTreeView, Grid, TreeRenderer, Search, _ActionMixin,
    SimpleQuery, Filter, DefaultActions, _Dialog, _Panel, WizardBase, Deferred, all, _Widget) {

        const ACTION = types.ACTION;
        const gridClass = Grid.createGridClass('driverTreeView', {
            showHeader: false,
            menuOrder: {
                'File': 110,
                'Edit': 100,
                'View': 90,
                'Block': 50,
                'Settings': 20,
                'Navigation': 10,
                'Step': 5,
                'New': 4,
                'Window': 1
            },
            options: utils.clone(types.DEFAULT_GRID_OPTIONS),
            _refreshInProgress: false,
            isGroup: function (item) {
                if (item) {
                    return item.isDir === true;
                } else {
                    return false;
                }
            },
            getRootFilter: function () {
                return {
                    parentId: ''
                };
            },
            getDefaultSort: function () {
                return [{ property: 'name', descending: false, ignoreCase: true }];
            },
            getDefaultCollection: function () {
                const _sort = this.getDefaultSort();
                return this.collection.sort(_sort);
            },
            postMixInProperties: function () {
                this.collection = this.store.filter(this.getRootFilter());
                this.collection = this.getDefaultCollection().filter(this.getRootFilter());
                this.inherited(arguments);
                this._on('onAddAction', function (action) {
                    if (action.command === types.ACTION.EDIT) {
                        action.label = 'Settings';
                    }
                });
            },
            runAction: function (action, item) {
                return this.inherited(arguments);
            },
            /**
             * Override render row to add additional CSS class
             * for indicating a block's enabled state
             * @param object
             * @returns {*}
             */
            renderRow: function (object) {
                const res = this.inherited(arguments), ref = object.ref, item = ref ? ref.item : null;

                if (item != null && item.scope && item.enabled === false) {
                    $(res).addClass('disabledBlock');
                }
                return res;
            }
        },
            //features
            {
                SELECTION: true,
                KEYBOARD_SELECTION: true,
                PAGINATION: false,
                ACTIONS: types.GRID_FEATURES.ACTIONS,
                CONTEXT_MENU: types.GRID_FEATURES.CONTEXT_MENU,
                TOOLBAR: types.GRID_FEATURES.TOOLBAR,
                CLIPBOARD: types.GRID_FEATURES.CLIPBOARD,
                SEARCH: {
                    CLASS: Search
                },
                WIDGET: {
                    CLASS: _Widget
                }
            },
            {
                RENDERER: TreeRenderer
            },
            null,
            null);
        /**
         * Driver tree view
         * @class module:xcf/views/DriverTreeView
         * @extends module:xide/widgets/_Widget
         * @extends module:xgrid/Base
         */
        const Module = declare('xcf.views.DriverGridView', gridClass, {
            permissions: [
                ACTION.RENAME,
                ACTION.RELOAD,
                ACTION.DELETE,
                ACTION.CLIPBOARD,
                ACTION.LAYOUT,
                ACTION.COLUMNS,
                ACTION.SELECTION,
                ACTION.TOOLBAR,
                ACTION.HEADER,
                ACTION.SEARCH,
                'File/ImportProtocol',
                ACTION.CONTEXT_MENU
            ],
            deselectOnRefresh: false,
            showHeader: false,
            toolbarInitiallyHidden: true,
            attachDirect: true,
            groupOrder: {
                'Clipboard': 110,
                'Device': 105,
                'File': 100,
                'Step': 80,
                'Open': 70,
                'Organize': 60,
                'Insert': 10,
                'New': 5,
                'Navigation': 3,
                'Select': 0
            },
            columns: [
                {
                    renderExpando: true,
                    label: "Name",
                    field: "name",
                    sortable: true,
                    formatter: function (name, item) {

                        if (this.grid.isGroup(item)) {
                            return '<span class="grid-icon ' + 'fa-folder' + '"></span>' + item.name;
                        }

                        if (item.iconClass) {
                            return '<span class="grid-icon ' + item.iconClass + '"></span>' + item.name;
                        }

                        if (item.icon) {
                            return '<span class="grid-icon ' + item.icon + '"></span>' + item.name;
                        }


                        if (!this.grid.isGroup(item)) {
                            const meta = item['user'];
                            const _in = meta ? utils.getCIInputValueByName(meta, types.DRIVER_PROPERTY.CF_DRIVER_NAME) : null;
                            if (meta) {
                                return '<span class="grid-icon ' + 'fa-exchange' + '"></span>' + _in;
                            } else {
                                return item.name;
                            }
                        } else {
                            return item.name;
                        }
                    }
                }
            ],
            _getActions: function (container, permissions, grid) {
                const actions = [],
                      thiz = this,
                      //grid = thiz,
                      delegate = thiz.delegate,
                      ACTION = types.ACTION,
                      item = thiz.getSelectedItem();

                const defaultMixin = {
                          addPermission: true,
                          quick: true
                      },
                      DEVICE_COMMAND_GROUP = 'Open';

                function _selection() {
                    const selection = thiz.getSelection();
                    if (!selection || !selection.length) {
                        return null;
                    }
                    const item = selection[0];
                    if (!item) {
                        console.error('have no item');
                        return null;
                    }
                    return selection;
                }

                function shouldDisableEdit() {
                    const selection = _selection(), item = selection ? selection[0] : null;
                    if (!item || item.isGroup) {
                        return true;
                    }
                    if (item.instances || (/*item.device && */item.blockScope)) {
                        return false;
                    }
                    return !(selection[0].device && selection[0].driver);
                }

                function shouldDisableConsole() {
                    const selection = _selection();
                    if (!selection || selection[0].isGroup) {
                        return true;
                    }

                    if ((selection[0].device && selection[0].driver)) {
                        return false;
                    }
                    return true;
                }

                function isRoot() {
                    const selection = _selection();
                    return !(!selection || selection.length === 0);
                }

                function isNotGroup() {
                    const selection = _selection();
                    return !(selection && selection[0] && selection[0].isDir);
                }


                actions.push(this.createAction('Edit', ACTION.EDIT, types.ACTION_ICON.EDIT, ['f4', 'enter', 'dblclick'], 'Home', 'Open', 'item', null,
                    function () {
                        return thiz.editItem(thiz.getSelectedItem());
                    },
                    {
                        addPermission: true,
                        tab: 'Home',
                        quick: true
                    }, null, shouldDisableEdit, permissions, container, grid
                ));

                actions.push(this.createAction('Code', 'File/Code', 'fa-code', ['shift f4'], 'Home', 'Open', 'item', null,
                    function () {
                        return delegate.editDriver(null, item);
                    },
                    {
                        addPermission: true,
                        tab: 'Home',
                        quick: true
                    }, null, shouldDisableEdit, permissions, container, grid
                ));

                actions.push(this.createAction('Console', 'File/Console', 'fa-terminal', ['f5', 'ctrl s'], 'Home', DEVICE_COMMAND_GROUP, 'item', null, null,
                    defaultMixin, null, shouldDisableConsole, permissions, container, grid
                ));

                actions.push(this.createAction('Log', 'File/Log', 'fa-calendar', ['f9'], 'Home', DEVICE_COMMAND_GROUP, 'item', null, null,
                    defaultMixin, null, shouldDisableConsole, permissions, container, grid
                ));

                actions.push(this.createAction('New Group', 'File/New Group', types.ACTION_ICON.NEW_DIRECTORY, ['f7'], 'Home', 'New', 'item|view', null, null,
                    defaultMixin, null, isRoot, permissions, container, grid
                ));

                actions.push(this.createAction('New Driver', 'File/New Driver', types.ACTION_ICON.NEW_FILE, ['f4'], 'Home', 'New', 'item', null, null,
                    defaultMixin, null, null, permissions, container, grid
                ));

                actions.push(this.createAction('Source', 'View/Source', 'fa-hdd-o', ['f4'], 'Home', 'Navigation', 'item', null,
                    function () {
                    },
                    {
                        addPermission: true,
                        tab: 'Home',
                        dummy: true
                    }, null, null, permissions, container, thiz
                ));

                actions.push(this.createAction('Import Protocol', 'File/ImportProtocol', 'fa-upload', null, 'Home', 'Navigation', 'item', null,
                    function () {
                    },
                    {
                        addPermission: true,
                        tab: 'Home',
                        quick: true
                    }, null, shouldDisableEdit, permissions, container, thiz
                ));

                let i = 0;
                //add source actions
                _.each([
                    {
                        label: 'System',
                        scope: 'system_drivers'
                    },
                    {
                        label: 'User',
                        scope: 'user_drivers'
                    }
                ], function (item) {
                    const label = item.label;
                    actions.push(thiz.createAction(label, 'View/Source/' + label, 'fa-hdd-o', ['alt f' + i], 'Home', 'Navigation', 'item', null,
                        function () {
                        },
                        {
                            addPermission: true,
                            tab: 'Home',
                            item: item,
                            quick: true
                        }, null, null, permissions, container, thiz
                    ));
                    i++;
                });
                return actions;
            },
            startup: function () {
                const _defaultActions = DefaultActions.getDefaultActions(this.permissions, this, this);

                this.inherited(arguments);

                const s = this._createContextMenu;
                if (!s) {
                    console.error('have no context menu!');
                }

                this.addActions(_defaultActions);

                this.addActions(this._getActions(this.domNode, this.permissions, this));

                const thiz = this;

                this.refresh().then(function () {
                    thiz.select([0], null, true, {
                        focus: true
                    });
                });

                this.subscribe(types.EVENTS.ON_STORE_CHANGED, function (evt) {
                    if (evt.store == thiz.collection) {
                        thiz.refresh();
                    }
                });
                this.wireStore(this.collection);

            },
            //////////////////////////////////////////////////////////////////////
            //
            //  Actions - Impl.
            //
            editItem: function (item) {
                const thiz = this, device = item.device, ctx = thiz.ctx, deviceManager = ctx.getDeviceManager();

                function edit() {
                    //instance
                    if (item.driver) {
                        item = item.driver;
                    }

                    thiz.openItem(item, device);
                }

                if (device && !device.driverInstance) {

                    deviceManager.startDevice(device).then(function () {
                        edit();
                    });
                }
                if (item.type === types.ITEM_TYPE.BLOCK) {
                    return thiz.blockManager.editBlock(item.ref.item, null, false);
                }
                edit();
            },
            wireStore: function (store) {
                const scope = store.scope;

                if (this['_storeConnected' + scope]) {
                    return;
                }

                this['_storeConnected' + scope] = true;

                const thiz = this;
                function updateGrid() {
                    setTimeout(function () {
                        thiz.refresh();
                    }, 100);
                }
                this.addHandle('update', store.on('update', updateGrid));
                this.addHandle('added', store.on('added', updateGrid));
                this.addHandle('remove', store.on('remove', updateGrid));
                this.addHandle('delete', store.on('delete', updateGrid));
            },
            //////////////////////////////////////////////////////////////////////
            //
            //  Actions - Impl.
            //
            clipboardPaste: function (items, target) {
                const dfd = new Deferred();
                const isCut = this.currentCutSelection,
                      self = this,
                      defaultDfdArgs = {
                          focus: true,
                          append: false
                      };
                items = items || isCut ? this.currentCutSelection : this.currentCopySelection;

                const allDfds = [], newItems = [];

                const head = new Deferred();

                target = target || this.getSelectedItem() || {};

                if (target.isDriver) {
                    target = target.getParent();
                }

                if (!target || !target.isDir) {
                    head.resolve();
                    return head;
                }

                items = items.filter(function (item) {
                    return item.isDriver;
                });

                if (!items || !items.length) {
                    head.resolve();
                    return head;
                }
                for (let i = 0; i < items.length; i++) {
                    const sub = self.delegate.cloneDriver(items[i], target);
                    //collect new items
                    sub.then(function (newItem) {
                        newItems.push(newItem);
                    });
                    allDfds.push(sub);
                }
                all(allDfds).then(function (sub) {
                    defaultDfdArgs.select = newItems;
                    head.resolve(defaultDfdArgs);
                });
                return head;
            },
            changeSource: function (source) {
                const self = this;
                const delegate = self.delegate;
                const scope = source.scope;
                let store = delegate.getStore(scope);

                function ready(store) {
                    self.collection = store;
                    const collection = store.filter(self.getRootFilter());
                    self.set('collection', collection);
                    self.set('title', 'Drivers (' + source.label + ')');
                    self.scope = scope;
                    self.wireStore(store);
                    self.refresh();
                }

                if (store.then) {
                    store.then(function () {
                        store = delegate.getStore(scope);
                        ready(store);
                    });
                } else {
                    //is store
                    ready(store);
                }
            },
            ///////////////////////////////////////////////////////////////
            runAction: function (action, _item) {
                if (!action || !action.command) {
                    return;
                }
                const ACTION = types.ACTION;
                const thiz = this;
                const delegate = thiz.delegate;
                const ctx = thiz.ctx;
                const deviceManager = ctx.getDeviceManager();
                let item = thiz.getSelectedItem() || _item;
                const device = item ? item.device : null;

                if (action.command.indexOf('View/Source') !== -1) {
                    return this.changeSource(action.item);
                }

                const defaultSelect = {
                    focus: true,
                    append: false,
                    delay: 1,
                    expand: true
                };

                switch (action.command) {
                    case 'File/Code': {
                        if (device && device.driverInstance) {
                            item = device.driverInstance.driver;
                        }
                        return delegate.editDriver(null, item);
                    }
                    case 'File/ImportProtocol': {
                        if (device && device.driverInstance) {
                            item = device.driverInstance.driver;
                        }
                        return this.importProtocol(item);
                    }
                    case 'File/Console':
                        {
                            if (device && !device.driverInstance) {
                                deviceManager.startDevice(device).then(function () {
                                    deviceManager.openConsole(item.device);
                                });
                                return;
                            }
                            return deviceManager.openConsole(item.device);
                        }
                    case 'File/Log':
                        {
                            return deviceManager.openLog(item.device);
                        }

                    case ACTION.EDIT:
                        {
                            return thiz.editItem(item);
                        }
                    case ACTION.RELOAD:
                        {
                            return thiz.refresh();
                        }
                    case ACTION.DELETE:
                        {
                            var res = delegate.onDeleteItem(item);
                            res.then(function () {
                                thiz.refresh();
                            });

                            return res;

                        }
                    case 'File/New Driver': {
                        const dfd = delegate.newItem(item);
                        dfd.then(function (newItem) {
                            try {
                                thiz.refresh();
                                if (newItem) {
                                    thiz.select(newItem, null, true, defaultSelect);
                                }
                            } catch (e) {
                                console.error('error!', e);
                            }
                        });
                        return dfd;
                    }
                    case 'File/New Group': {
                        var res = delegate.newGroup(item, thiz.scope);
                        res.then(function (item) {
                            thiz.refresh();
                            thiz.select(item, null, true, defaultSelect);
                        });
                        return res;
                    }
                }
                return this.inherited(arguments);
            },
            importProtocol: function (driver) {
                const ctx = this.ctx;
                let selectedItems = [], selectedItem = null, wizard = null, activePane;

                function createProtocolView(parent, ctx, driver, device, type, mixin) {
                    return utils.addWidget(ProtocolTreeView, utils.mixin({
                        title: 'Devices',
                        collection: ctx.getProtocolManager().store,
                        delegate: ctx.getProtocolManager(),
                        blockManager: ctx.getBlockManager(),
                        ctx: ctx,
                        icon: 'fa-sliders',
                        showHeader: false,
                        scope: 'system_protocols',
                        resizeToParent: true
                    }, mixin), null, parent, true);                    
                }

                function createProtocolWizard(parent, ctx, driver, device, finish) {
                    const args = {
                        onFinish: function () {
                            const variableProto = utils.getObject('xcf/model/Variable'), commandProto = utils.getObject('xcf/model/Command'), scope = driver.blockScope, items = selectedItems;

                            for (let i = 0; i < items.length; i++) {
                                const protocolItem = items[i];
                                const isVariable = protocolItem.type == 'protocolVariable';
                                const name = protocolItem.name || protocolItem.title;
                                let ctrArgs = null;
                                if (isVariable) {
                                    ctrArgs = {
                                        value: protocolItem.value,
                                        name: name,
                                        group: 'basicVariables'

                                    }
                                } else {
                                    ctrArgs = {
                                        send: protocolItem.value,
                                        name: name,
                                        group: 'basic'
                                    }
                                }

                                const existing = scope.getBlockByName(name) || scope.getVariable(name);
                                if (existing) {
                                    isVariable ? (existing.value = protocolItem.value) : (existing.send = protocolItem.value);
                                    for (const prop in protocolItem) {
                                        if (prop !== 'name' && prop !== 'value' && prop !== 'title') {
                                            existing[prop] = protocolItem[prop];
                                        }
                                    }
                                    existing.refresh();
                                    finish && finish();
                                    return;
                                }
                                const _proto = isVariable ? variableProto : commandProto;
                                for (const prop0 in protocolItem) {
                                    if (prop0 !== 'name' && prop0 !== 'value' && prop0 !== 'title') {
                                        ctrArgs[prop0] = protocolItem[prop0];
                                    }
                                }
                                let block = factory.createBlock(_proto, ctrArgs);
                                block = scope.blockStore.putSync(block);
                                block.scope = scope;
                                finish && finish();
                            }
                        },
                        options: {
                            tabClass: 'nav',
                            'nextSelector': '.button-next',
                            'previousSelector': '.button-previous',
                            'firstSelector': '.button-first',
                            'lastSelector': '.button-last',
                            'finishSelector': '.button-finish',
                            'backSelector': '.button-back'
                        },
                        panes: [
                            {
                                title: "Select Protocol",
                                icon: "fa-hand-o-up",
                                selected: true,
                                onShow: function (pane) {
                                    activePane = pane;
                                    wizard && wizard.hideButton('.button-finish', true);
                                },
                                create: function (parent, pane) {

                                    const view = createProtocolView(parent, ctx, driver, device, 'command');
                                    pane.widget = view;
                                    view._on('selectionChanged', function (evt) {
                                        const selection = evt.selection;
                                        selectedItem = selection[0];
                                        wizard.disableButton('.button-next', (!selectedItem || selectedItem.virtual));
                                    });
                                    return view;
                                }
                            },
                            {
                                title: "Select Items",
                                icon: "fa-hand-o-up",
                                onShow: function (pane) {

                                    activePane = pane;

                                    wizard.disableButton('.button-next', true);
                                    wizard.hideButton('.button-finish', false);
                                },
                                create: function (parent, pane) {

                                    const view = createProtocolView(parent, ctx, driver, device, 'command', {
                                        getRootFilter: function () {
                                            if (selectedItem) {
                                                return {
                                                    parentId: selectedItem.path
                                                }
                                            } else {
                                                return {
                                                    parentId: ''
                                                }
                                            }
                                        }
                                    });
                                    pane.widget = view;

                                    view._on('selectionChanged', function (evt) {
                                        const selection = evt.selection;
                                        selectedItems = selection;
                                        wizard.disableButton('.button-finish', (!selection.length || selection[0].isDir));

                                    });
                                    wizard.hideButton('.button-finish', false);

                                    return view;
                                }
                            }
                        ],
                        buttons: [
                            {
                                title: 'Previous',
                                cls: 'btn btn-default button-previous pull-left',
                                name: 'previous',
                                value: 'Previous',
                                disabled: true
                            },
                            {
                                title: 'Finish',
                                cls: 'btn btn-default button-finish pull-right',
                                name: 'finish',
                                value: 'Finish'
                            },
                            {
                                title: 'Next',
                                cls: 'btn btn-default button-next pull-right',
                                name: 'next',
                                value: 'Next',
                                disabled: true
                            }
                        ]
                    };
                    return utils.addWidget(WizardBase, args, null, parent, true);
                }

                function createProtocolWizardDialog(parent, ctx, driver, device, type) {

                    const panel = new _Panel({
                        title: 'Import protocol ',
                        options: {
                            "contentSize": {
                                width: '600px',
                                height: '500px'
                            }
                        },
                        onShow: function (panel, contentNode) {
                            //return [FTestUtils.createFileGrid(null,{},null,null,null,true,contentNode)];
                            const self = this;
                            wizard = createProtocolWizard(contentNode, ctx, driver, device, function () {
                                self.destroy();
                            });
                            return [wizard];
                        }
                    });

                    panel.show();
                    return panel;

                    /*
                    var dlg = new _Dialog({
                        title: 'Import protocol ',
                        type:types.DIALOG_TYPE.INFO,
                        bodyCSS: {
                            'height': '500px',
                            'min-height': '300px',
                            'padding': '4px',
                            'margin-right': '0px'
                        },
                        buttons:[],
                        picker: null,
                        onOk: function () {
                        },
    
                        message: function (dlg) {
                            var thiz = dlg.owner;
                            var picker = createProtocolWizard($('<div class="" style="height: inherit;" />')[0],ctx,driver,device,function(){
                                dlg.owner.destroy();
                            });
                            thiz.picker = picker;
                            wizard = picker;
                            thiz.add(picker, null, false);
                            return $(picker.domNode);
                        },
                        onShow: function () {
                            var picker = this.picker;
                            picker.startup();
                            this.startDfd.resolve();
                        }
                    });
    
    
    
    
                    dlg.show().then(function () {
                        dlg.resize();
                        utils.resizeTo(dlg.picker, dlg, true, true);
                    });
    
                    */
                }

                return createProtocolWizardDialog(null, ctx, driver, null, 'command', true);

            },
            createProtocolWizard: function () {


                /*
                var thiz = this,
                    selectView = null,
                    selectItemsView = null,
                    selectedItems = [],
                    selectedItem = null,
                    driver = thiz.getSelectedItem(),
                    protocolMgr = thiz.delegate.ctx.getProtocolManager(),
                    store = protocolMgr.getStore();
    
                var wizardStruct = Wizards.createWizard('Select Protocol', false, {});
    
                this._lastWizard = wizardStruct;
                var wizard = wizardStruct.wizard;
                var dialog = wizardStruct.dialog;
    
    
                var done = function () {
                    wizard.destroy();
                    dialog.destroy();
                    thiz.importProtocol(selectedItem, selectedItems, driver);
                }
    
                dialog.show().then(function () {
    
    
                    selectView = utils.addWidget(ProtocolTreeView, {
                        title: 'Protocols',
                        store: protocolMgr.getStore(),
                        delegate: protocolMgr,
                        beanContextName: '3',// thiz.ctx.mainView.beanContextName
                        _doneFunction: function () {
                            return false;
                        },
                        passFunction: function () {
                            return true;
                        },
                        canGoBack: true,
                        gridParams: {
                            showHeader: true
                        },
                        editItem: function () {
    
                            if (!this.isGroup(selectedItem)) {
                                wizard._forward();
                            }
                        }
    
                    }, protocolMgr, wizard, true, null, [WizardPaneBase]).
                        _on(types.EVENTS.ON_ITEM_SELECTED, function (evt) {
                            selectedItem = evt.item;
                            wizard.adjustWizardButton('next', selectView.isGroup(evt.item));
    
                        });
    
    
                    selectItemsView = utils.addWidget(ProtocolTreeView, {
                        title: 'Protocols',
                        store: protocolMgr.getStore(),
                        delegate: protocolMgr,
                        beanContextName: '3',// thiz.ctx.mainView.beanContextName
                        doneFunction: function () {
                            done();
                            return true;
                        },
                        passFunction: function () {
                            //not used
                            return true;
                        },
                        canGoBack: true,
                        editItem: function () {
    
                        },
                        getRootFilter: function () {
                            if (selectedItem) {
                                return {
                                    parentId: selectedItem.path
                                }
                            } else {
                                return {
                                    parentId: ''
                                }
                            }
                        },
                        canSelect: function (item) {
                            if (item.virtual === true && item.isDir === false) {
                                return true;
                            }
                            return false;
                        }
                    }, protocolMgr, wizard, true, null, [WizardPaneBase], null, {
                        getColumns: function () {
    
                            var _res = this.inherited(arguments);
                            _res.push({
                                label: "Value",
                                field: "value",
                                sortable: false,
                                _formatter: function (name, item) {
    
                                    if (!thiz.isGroup(item) && item['user']) {
                                        var meta = item['user'].meta;
                                        var _in = meta ? utils.getCIInputValueByName(meta, types.PROTOCOL_PROPERTY.CF_PROTOCOL_TITLE) : null;
                                        if (meta) {
                                            return '<span class="grid-icon ' + 'fa-exchange' + '"></span>' + _in;
                                        } else {
                                            return item.name;
                                        }
                                        return _in;
                                    } else {
                                        return item.name;
                                    }
                                }
                            });
                            return _res;
                        }
                    }).
                        _on(types.EVENTS.ON_ITEM_SELECTED, function (evt) {
                            selectedItems = selectItemsView.getSelection();
                            wizard.adjustWizardButton('done', selectedItems.length == 0);
    
                        });
    
    
                    wizard.onNext = function () {
                        dialog.set('title', 'Select Commands & Variables');
                        selectItemsView.grid.set('collection', store.filter(selectItemsView.getRootFilter()));
                        selectItemsView.grid.refresh();
                    }
                });
    
                */

            },
            changeScope: function (name) {
                this.delegate.ls(name + '_drivers').then(function (data) {
                });
            },
            openItem: function (item, device) {
                if (item && !item.isDir && !item.virtual) {
                    this.delegate.openItemSettings(item, device);
                }
            }
        });
        //Module.DefaultPermissions = DefaultPermissions;
        Module.getActions = Module.prototype._getActions;
        return Module;
    });