define([
    "dcl/dcl",
    "xdojo/has",
    "xide/model/Component",
    "xide/types"
], function (dcl,has,Component,types) {
    /**
     *
     * Implement component interface basics
     *
     * @class xide.component
     * @inheritDoc
     */
    return dcl(Component, {
        /**
         * @inheritDoc
         */
        declaredClass:'xconsole.component',
        /**
         * @inheritDoc
         */
        beanType:'expression',
        //////////////////////////////////////////////////////////////////////////////////////////////////////
        //
        //  Implement base interface
        //
        //////////////////////////////////////////////////////////////////////////////////////////////////////
        hasEditors:function(){
            return ['xconsole','xexpression','xshell'];
        },
        getDependencies:function(){
            if(has('host-browser')) {
                return [
                ];
            }else{
                return [
                    'xide/xide'
                ];
            }
        },
        /**
         * @inheritDoc
         */
        getLabel: function () {
            return types.COMPONENT_NAMES.XCONSOLE;
        },
        /**
         * @inheritDoc
         */
        getBeanType:function(){
            return types.ITEM_TYPE.EXPRESSION;
        }
    });
});

