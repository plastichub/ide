define([
    'dummy/utils'
], function (utils){
    /**
     *
     * @param arr
     * @param val
     * @returns {number}
     */
    utils.contains=function(arr,val){
        for (var i = 0; i < arr.length; i++) {
            if(arr[i]===val){
                return i;
            }
        }
        return -1;
    };
    /**
     *
     * @param obj
     * @param val
     * @returns {*}
     */
    var getObjectKeyByValue=function(obj,val){
        if(obj && val) {
            for (var prop in obj) {
                if (obj.hasOwnProperty(prop)) {
                    if (obj[prop] === val)
                        return prop;
                }
            }
        }
        return null;
    };
    return utils;
});
