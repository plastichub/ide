define([
    "dojo/_base/kernel",
    'wcDocker/docker',
    'wcDocker/splitter',
    'wcDocker/frame',
    'wcDocker/collapser',
    'wcDocker/layoutsimple',
    'wcDocker/layouttable',
    'wcDocker/tabframe',
    'wcDocker/panel',
    'wcDocker/docker',
    'wcDocker/ghost',
    'wcDocker/base',
    'wcDocker/drawer',
    'wcDocker/iframe',
    'wcDocker/types'
], function(dojo){
    return dojo.wcDocker;
});
