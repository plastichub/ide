## Modules

<dl>
<dt><a href="#module_xtrack/component">xtrack/component</a></dt>
<dd></dd>
<dt><a href="#module_xtrack/data/Store">xtrack/data/Store</a></dt>
<dd></dd>
<dt><a href="#module_xtrack/manager/TrackingManager">xtrack/manager/TrackingManager</a></dt>
<dd></dd>
<dt><a href="#module_xtrack/model/Item">xtrack/model/Item</a></dt>
<dd></dd>
</dl>

## Classes

<dl>
<dt></dt>
<dd></dd>
</dl>

<a name="module_xtrack/component"></a>

## xtrack/component

* [xtrack/component](#module_xtrack/component)
    * [~beanType](#module_xtrack/component..beanType)
    * [~getLabel()](#module_xtrack/component..getLabel)
    * [~run(ctx)](#module_xtrack/component..run)

<a name="module_xtrack/component..beanType"></a>

### xtrack/component~beanType
**Kind**: inner property of <code>[xtrack/component](#module_xtrack/component)</code>  
<a name="module_xtrack/component..getLabel"></a>

### xtrack/component~getLabel()
**Kind**: inner method of <code>[xtrack/component](#module_xtrack/component)</code>  
<a name="module_xtrack/component..run"></a>

### xtrack/component~run(ctx)
**Kind**: inner method of <code>[xtrack/component](#module_xtrack/component)</code>  

| Param | Type |
| --- | --- |
| ctx | <code>module:xide/manager/ContextBase</code> | 

<a name="module_xtrack/data/Store"></a>

## xtrack/data/Store

* [xtrack/data/Store](#module_xtrack/data/Store)
    * 
        * [~removeSync(url)](#module_xtrack/data/Store--undefined..removeSync) ⇒ <code>boolean</code>

<a name="exp_module_xtrack/data/Store--undefined"></a>

### 
**Kind**: global class of <code>[xtrack/data/Store](#module_xtrack/data/Store)</code>  
**Extends:** <code>module:xide/data/TreeMemory</code>, <code>module:xide/data/ObservableStore</code>, <code>module:dstore/Trackable</code>  
<a name="module_xtrack/data/Store--undefined..removeSync"></a>

#### ~removeSync(url) ⇒ <code>boolean</code>
Override dstore/Memory for removing child tracks as well.

**Kind**: inner method of <code>[undefined](#exp_module_xtrack/data/Store--undefined)</code>  

| Param | Type |
| --- | --- |
| url | <code>string</code> | 

<a name="module_xtrack/manager/TrackingManager"></a>

## xtrack/manager/TrackingManager

* [xtrack/manager/TrackingManager](#module_xtrack/manager/TrackingManager)
    * [~STORE_CLASS](#module_xtrack/manager/TrackingManager..STORE_CLASS) : <code>[undefined](#exp_module_xtrack/data/Store--undefined)</code> &#124; <code>module:xide/data/_Base</code>
    * [~MODEL_CLASS](#module_xtrack/manager/TrackingManager..MODEL_CLASS) : <code>[xtrack/model/Item](#module_xtrack/model/Item)</code> &#124; <code>module:xide/data/Model</code>
    * [~_store](#module_xtrack/manager/TrackingManager.._store) : <code>[undefined](#exp_module_xtrack/data/Store--undefined)</code>
    * [~getStore()](#module_xtrack/manager/TrackingManager..getStore) ⇒ <code>[undefined](#exp_module_xtrack/data/Store--undefined)</code>
    * [~track(category, label, url, command, context)](#module_xtrack/manager/TrackingManager..track) ⇒ <code>[xtrack/model/Item](#module_xtrack/model/Item)</code>
    * [~init()](#module_xtrack/manager/TrackingManager..init)
    * [~serialize()](#module_xtrack/manager/TrackingManager..serialize)
    * [~deserialize(data)](#module_xtrack/manager/TrackingManager..deserialize)
    * [~current()](#module_xtrack/manager/TrackingManager..current) ⇒ <code>\*</code>
    * [~destroy()](#module_xtrack/manager/TrackingManager..destroy)

<a name="module_xtrack/manager/TrackingManager..STORE_CLASS"></a>

### xtrack/manager/TrackingManager~STORE_CLASS : <code>[undefined](#exp_module_xtrack/data/Store--undefined)</code> &#124; <code>module:xide/data/_Base</code>
Override store class

**Kind**: inner property of <code>[xtrack/manager/TrackingManager](#module_xtrack/manager/TrackingManager)</code>  
<a name="module_xtrack/manager/TrackingManager..MODEL_CLASS"></a>

### xtrack/manager/TrackingManager~MODEL_CLASS : <code>[xtrack/model/Item](#module_xtrack/model/Item)</code> &#124; <code>module:xide/data/Model</code>
Override item model class

**Kind**: inner property of <code>[xtrack/manager/TrackingManager](#module_xtrack/manager/TrackingManager)</code>  
<a name="module_xtrack/manager/TrackingManager.._store"></a>

### xtrack/manager/TrackingManager~_store : <code>[undefined](#exp_module_xtrack/data/Store--undefined)</code>
**Kind**: inner property of <code>[xtrack/manager/TrackingManager](#module_xtrack/manager/TrackingManager)</code>  
<a name="module_xtrack/manager/TrackingManager..getStore"></a>

### xtrack/manager/TrackingManager~getStore() ⇒ <code>[undefined](#exp_module_xtrack/data/Store--undefined)</code>
**Kind**: inner method of <code>[xtrack/manager/TrackingManager](#module_xtrack/manager/TrackingManager)</code>  
<a name="module_xtrack/manager/TrackingManager..track"></a>

### xtrack/manager/TrackingManager~track(category, label, url, command, context) ⇒ <code>[xtrack/model/Item](#module_xtrack/model/Item)</code>
**Kind**: inner method of <code>[xtrack/manager/TrackingManager](#module_xtrack/manager/TrackingManager)</code>  

| Param | Type |
| --- | --- |
| category | <code>string</code> | 
| label | <code>string</code> | 
| url | <code>string</code> | 
| command | <code>string</code> &#124; <code>null</code> | 
| context | <code>string</code> &#124; <code>null</code> | 

<a name="module_xtrack/manager/TrackingManager..init"></a>

### xtrack/manager/TrackingManager~init()
impl. std interface

**Kind**: inner method of <code>[xtrack/manager/TrackingManager](#module_xtrack/manager/TrackingManager)</code>  
<a name="module_xtrack/manager/TrackingManager..serialize"></a>

### xtrack/manager/TrackingManager~serialize()
impl. std interface

**Kind**: inner method of <code>[xtrack/manager/TrackingManager](#module_xtrack/manager/TrackingManager)</code>  
<a name="module_xtrack/manager/TrackingManager..deserialize"></a>

### xtrack/manager/TrackingManager~deserialize(data)
impl. std interface

**Kind**: inner method of <code>[xtrack/manager/TrackingManager](#module_xtrack/manager/TrackingManager)</code>  

| Param | Type |
| --- | --- |
| data | <code>string</code> &#124; <code>Array.&lt;object&gt;</code> | 

<a name="module_xtrack/manager/TrackingManager..current"></a>

### xtrack/manager/TrackingManager~current() ⇒ <code>\*</code>
std query

**Kind**: inner method of <code>[xtrack/manager/TrackingManager](#module_xtrack/manager/TrackingManager)</code>  
<a name="module_xtrack/manager/TrackingManager..destroy"></a>

### xtrack/manager/TrackingManager~destroy()
**Kind**: inner method of <code>[xtrack/manager/TrackingManager](#module_xtrack/manager/TrackingManager)</code>  
<a name="module_xtrack/model/Item"></a>

## xtrack/model/Item

* [xtrack/model/Item](#module_xtrack/model/Item)
    * [~label](#module_xtrack/model/Item..label) : <code>string</code>
    * [~context](#module_xtrack/model/Item..context) : <code>string</code> &#124; <code>null</code>
    * [~category](#module_xtrack/model/Item..category) : <code>string</code>
    * [~url](#module_xtrack/model/Item..url) : <code>url</code>
    * [~parent](#module_xtrack/model/Item..parent) : <code>url</code>
    * [~id](#module_xtrack/model/Item..id) : <code>string</code>
    * [~command](#module_xtrack/model/Item..command) : <code>string</code> &#124; <code>null</code>
    * [~time](#module_xtrack/model/Item..time) : <code>integer</code>
    * [~counter](#module_xtrack/model/Item..counter) : <code>integer</code>
    * [~_history](#module_xtrack/model/Item.._history) : <code>module:xide/views/History</code>
    * [~retain()](#module_xtrack/model/Item..retain) ⇒ <code>number</code>
    * [~getHistory()](#module_xtrack/model/Item..getHistory) ⇒ <code>module:xide/views/History</code>

<a name="module_xtrack/model/Item..label"></a>

### xtrack/model/Item~label : <code>string</code>
An ui friendly label.

**Kind**: inner property of <code>[xtrack/model/Item](#module_xtrack/model/Item)</code>  
<a name="module_xtrack/model/Item..context"></a>

### xtrack/model/Item~context : <code>string</code> &#124; <code>null</code>
Additional filter vector.

**Kind**: inner property of <code>[xtrack/model/Item](#module_xtrack/model/Item)</code>  
<a name="module_xtrack/model/Item..category"></a>

### xtrack/model/Item~category : <code>string</code>
An ui friendly category. We may want to group tracking along this vector.

**Kind**: inner property of <code>[xtrack/model/Item](#module_xtrack/model/Item)</code>  
<a name="module_xtrack/model/Item..url"></a>

### xtrack/model/Item~url : <code>url</code>
An unique url.

**Kind**: inner property of <code>[xtrack/model/Item](#module_xtrack/model/Item)</code>  
<a name="module_xtrack/model/Item..parent"></a>

### xtrack/model/Item~parent : <code>url</code>
An unique url to the parent's tracking url.

**Kind**: inner property of <code>[xtrack/model/Item](#module_xtrack/model/Item)</code>  
<a name="module_xtrack/model/Item..id"></a>

### xtrack/model/Item~id : <code>string</code>
A random uuid.

**Kind**: inner property of <code>[xtrack/model/Item](#module_xtrack/model/Item)</code>  
<a name="module_xtrack/model/Item..command"></a>

### xtrack/model/Item~command : <code>string</code> &#124; <code>null</code>
We track the action origin so we're able to determine more frequent used actions.

**Kind**: inner property of <code>[xtrack/model/Item](#module_xtrack/model/Item)</code>  
<a name="module_xtrack/model/Item..time"></a>

### xtrack/model/Item~time : <code>integer</code>
The time of usage.

**Kind**: inner property of <code>[xtrack/model/Item](#module_xtrack/model/Item)</code>  
<a name="module_xtrack/model/Item..counter"></a>

### xtrack/model/Item~counter : <code>integer</code>
The counter of usage.

**Kind**: inner property of <code>[xtrack/model/Item](#module_xtrack/model/Item)</code>  
<a name="module_xtrack/model/Item.._history"></a>

### xtrack/model/Item~_history : <code>module:xide/views/History</code>
To track the usage in a time table in order to determine 'frequent' actions.

**Kind**: inner property of <code>[xtrack/model/Item](#module_xtrack/model/Item)</code>  
<a name="module_xtrack/model/Item..retain"></a>

### xtrack/model/Item~retain() ⇒ <code>number</code>
Retain usage counter and track time of usage.

**Kind**: inner method of <code>[xtrack/model/Item](#module_xtrack/model/Item)</code>  
<a name="module_xtrack/model/Item..getHistory"></a>

### xtrack/model/Item~getHistory() ⇒ <code>module:xide/views/History</code>
**Kind**: inner method of <code>[xtrack/model/Item](#module_xtrack/model/Item)</code>  
