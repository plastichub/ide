/*global module */
module.exports = function (grunt) {
    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON("package.json"),
        jsdoc2md: {
            oneOutputFile: {
                src: 'src/**/*.js',
                dest: 'api/documentation.md'
            }
        },
        "jsdoc-amddcl": {
            "plugins": [
                "plugins/markdown"
            ],
            docs: {
                files: [
                    {
                        src: [
                            "src/**/*.js",
                            "!src/component",
                            "!./node_modules"
                        ]
                    }
                ]
            },
            'export': {
                files: [
                    {
                        args: [
                            "-X"
                        ],
                        src: [
                            "./"
                        ],
                        dest: "/tmp/doclets_xtrack.json"
                    }
                ]
            }
        }
    });

    // Load plugins
    grunt.loadNpmTasks("jsdoc-amddcl");
    grunt.loadNpmTasks('grunt-jsdoc-to-markdown');
    // Aliases
    //grunt.registerTask("css", ["less", "cssToJs"]);
    grunt.registerTask("jsdoc", "jsdoc-amddcl");
    grunt.registerTask("api", "jsdoc2md");
};