/**
 * @module xtrack/manager/TrackingManager
 */
define([
    'dcl/dcl',
    'xide/manager/ManagerBase',
    'xide/utils',
    "xtrack/model/Item",
    "xtrack/data/Store",
    "xide/manager/ServerActionBase"
], function (dcl, ManagerBase, utils, Item, Store, ServerActionBase) {


    /**
     *
     * Goals:
     *
     * - tracking of actions & item usage for building a history of user activities
     * - being able to retrieve a user's most used items
     * - being able to retrieve a user's most done actions
     *
     * @class module:xide/manager/TrackingManager
     * @extends module:xide/manager/ManagerBase
     */
    return dcl([ManagerBase, ServerActionBase], {
        declaredClass: "xtrack.manager.TrackingManager",
        /**
         * server RPC class
         */
        serviceClass: 'XApp_Tracking_Service',
        /**
         * Section in store to track our items
         * $type {string}
         */
        section: 'meta',
        /**
         * @type {module:xtrack/data/Store|module:xide/data/_Base}
         */
        STORE_CLASS: Store,
        /**
         * @type {module:xtrack/model/Item|module:xide/data/Model}
         */
        MODEL_CLASS: Item,
        /**
         * @type {module:xtrack/data/Store}
         */
        _store: null,
        /**
         *
         * @returns {module:xtrack/data/Store}
         */
        getStore: function () {
            return this._store;
        },
        last: function (items) {
            items = items || this.getStore().query();
            items = items.sort(function (a, b) {
                return (a.time || 1) > (b.time || 1) ? -1 : 1;
            });
            return items;
        },
        most: function (query, items) {
            items = items || this.getStore().query(query);
            items = items.sort(function (a, b) {
                if (a.counter === b.counter) {
                    return 0;
                }
                return (a.counter || 1) < (b.counter || 1) ? 1 : -1
            });
            return items;
        },
        filterMin: function (query, prop, min) {
            var items = this.getStore().query(query);
            items = items.filter(function (item) {
                return item[prop] >= min;
            });
            return items;
        },
        /**
         *
         * @param category {string}
         * @param label {string}
         * @param url {string}
         * @param command {string|null}
         * @param context {string|null}
         * @param mixin {object|null}
         * @returns {module:xtrack/model/Item}
         */
        track: function (category, label, url, command, context, mixin) {
            var storeItem = this._store.getSync(url);
            if (storeItem) {
                storeItem.retain();
                this.save();
                this._store.emit('update', {target: storeItem});
                return storeItem;
            }
            storeItem = utils.mixin({
                id: utils.createUUID(),
                time: new Date().getTime(),
                url: url,
                label: label,
                category: category,
                counter: 1,
                command: command,
                context: context
            }, mixin);
            var item = this._store.putSync(storeItem);
            this.save();
            return item;
        },
        drop: function (url) {
            this._store.removeSync(url);
        },
        _init:function(){
            this.check();
            return this._read(this.section, '.', null, function (data) {
                this._store = new this.STORE_CLASS({
                    Model: this.MODEL_CLASS
                });
                data = _.find(data.meta, {id: 'tracking'});
                data = data || {value: []};
                this._store.setData(data.value || []);
            }.bind(this));
        },
        /**
         * impl. std interface
         * @inheritDoc
         */
        init: function () {
            return this._init();
        },
        save: function () {
            return this.runDeferred(null, 'set', [this.section, '.', {id: 'tracking'}, {value: this.current()}, true]);
        },
        _read: function (section, path, query, readyCB) {
            return this.runDeferred(null, 'get', [section, path, query]).then(function (data) {
                readyCB && readyCB(data);
            }.bind(this));
        },
        /**
         * impl. std interface
         * @inheritDoc
         */
        serialize: function () {
            return JSON.stringify(this.current(), null, 2);
        },
        /**
         * impl. std interface
         * @param data {string|object[]}
         * @inheritDoc
         */
        deserialize: function (data) {
            this._store.setData(utils.fromJson(data));
        },
        /**
         * std query
         * @returns {*}
         */
        current: function () {
            return this._store.query();
        },
        /**
         * @inheritDoc
         */
        destroy: function () {
            this._store.destroy();
        }
    });
});
