#!/bin/bash

# Base directory for this entire project
BASEDIR=$(cd $(dirname $0) && pwd)

# Source directory for unbuilt code
SRCDIR="$BASEDIR/lib"

# Directory containing dojo build utilities
TOOLSDIR="$SRCDIR/util/buildscripts"

# Destination directory for built code
DISTDIR="$BASEDIR/xfileBuild"

DISTDIR_FINAL="$BASEDIR/xfile"

rm -rf $DISTDIR/

# Module ID of the main application package loader configuration
LOADERMID="xbox/run-release"

# Main application package loader configuration
LOADERCONF="$SRCDIR/$LOADERMID.js"

# Main application package build configuration
PROFILE="$SRCDIR/xbox/app.profile.js"



# Configuration over. Main application start up!

if [ ! -d "$TOOLSDIR" ]; then
    echo "Can't find Dojo build tools -- did you initialise submodules? (git submodule update --init --recursive)"
    exit 1
fi

echo "Building application with $PROFILE to $DISTDIR."

echo -n "Cleaning old files..."
rm -rf "$DISTDIR"
echo " Done"


rm -rf $SRCDIR/xbox/css
cp -rf $BASEDIR/css $SRCDIR/xbox/css

rm -rf $SRCDIR/xbox/xfileTheme
cp -rf $BASEDIR/xfileTheme $SRCDIR/xbox/xfileTheme
rm -rf $SRCDIR/xbox/css/xcf

rm -rf $SRCDIR/xbox/css/xfileTheme/tundra/
rm -rf $SRCDIR/xbox/css/xfileTheme/nihilo/
rm -rf $SRCDIR/xbox/css/xfileTheme/soria/


########################################################################################
#
#   Create Symbolic Files to main CSS directories
#

cd "$TOOLSDIR"

if which java >/dev/null; then
    java -Xms1024m -Xmx2048m  -cp ../shrinksafe/js.jar:../closureCompiler/compiler.jar:../shrinksafe/shrinksafe.jar org.mozilla.javascript.tools.shell.Main  ../../dojo/dojo.js baseUrl=../../dojo load=build --require "$LOADERCONF" --profile "$PROFILE" --releaseDir "$DISTDIR" "$@"
else
    echo "Need node.js or Java to build!"
    exit 1
fi
#
#
#
########################################################################################

cd "$BASEDIR"
echo "Build complete"

########################################################################################
#
#   Copy dojo i8ln files
#
cp -rf "$DISTDIR/dojo/nls/" $DISTDIR/nls

######################
#
#   Clean up temp files
#

cp -rf $DISTDIR/dojo/dojo.js $BASEDIR/xfile/dojo/xbox.js
#cp -rf $DISTDIR/dojo/dojo.js.uncompressed.js $BASEDIR/xfile/dojo/xbox.js

find $DISTDIR -name *.uncompressed.js -exec rm '{}' ';'
find $DISTDIR -name *.consoleStripped.js -exec rm '{}' ';'
find $DISTDIR -name *.map -exec rm '{}' ';'
find $DISTDIR -name *.svn -exec rm -rf '{}' ';'



rm -rf $BASEDIR/xfile/xbox/images
cp -rf $BASEDIR/images $BASEDIR/xfile/xbox/images

rm -rf $BASEDIR/xfile/images/xas
mkdir -p $BASEDIR/xfile/images/
cp -rf $BASEDIR/images/xas $BASEDIR/xfile/images/xas

rm -rf $BASEDIR/xfile/xideve
cp -rf $DISTDIR/xideve $BASEDIR/xfile/xideve



cp -rf $SRCDIR/orion-release/orion/* $BASEDIR/xfile/orion/

cp -rf $SRCDIR/davinci-release/davinci/* $BASEDIR/xfile/davinci/

cp -rf $SRCDIR/system $BASEDIR/xfile/system
cp -rf $SRCDIR/preview $BASEDIR/xfile/preview

cp -rf $DISTDIR/xbox/resources/app.css $BASEDIR/xfile/xbox/resources/app.css

#
# POST STEPS FINISH
#
########################################################################################
