#!/usr/bin/env bash
gitc "$1"
git-module commit-modules --message=$1
git-module commit-modules --profile="control-freak" --message=$1