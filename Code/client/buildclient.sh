#!/usr/bin/env bash
grunt build-xapp --verbose
grunt buildXFILE --package=xfile --verbose
grunt cssmin:xcf --verbose
cd src
sh buildxcf.sh
sh buildXAppExternals.sh
sh minifyxapp.sh
