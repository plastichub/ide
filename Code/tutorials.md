var vol = [Volume] - 2;
return "mv" + vol;

var vol = [Volume] + 2;
return "mv" + vol;


updateVolume: function (value) {

            var out = 0;
            
            
            
            //check we have 'MV' in the house and make sure 'MVMAX' is not 
            if (value.indexOf('MV') != -1 && value.indexOf('MVMAX') == -1) {
            	
            	
            	//grab all from 2 chars ahead
              var _volume = value.substring(2, value.length);
                
              //cast to integer
              _volume = parseInt(_volume.substring(0, 2));
              
              //if valid, store as variable
              if (!isNaN(_volume)) {
                  
                  this.setVariable('Volume', _volume);
                  out = _volume;
                  
              }
            }
            return out;

        },
updatePower: function (value) {
                
                var out = 0;
    
    if (value.indexOf('@PWR:') != -1) {
        
        var _pw = value.split(':')[1];
        
        if (!isNaN(_pw)) {
            
            this.setVariable('PowerState', _pw == 2 ? 'on' : 'off');
            
            out = _pw;
        }
        
    } 
    return out;

},

