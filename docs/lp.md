---- Espanol -----------------------------------------------------------------------------
Apelo aquí la sentencia &quot;50/2017 T&quot; - 1/12/2017 por las siguientes razones:
- &#39;Marc Franquesa Aran&#39; me mantuvo contra mi voluntad durante 45 minutos en La-
Porkera sin ningún motivo. Hasta donde yo entendí, él me mantuvo contra mi voluntad en
La-Porkera para esperar a Hector-Taus, que destruía o robara mis materiales /
propiedades en repetidas ocasiones. Héctor también me golpeó físicamente
anteriormente. No tuve más remedio que huir de la escena a toda costa. ¡Actué por
autodefensa!
- ¡No lastimé a Franq con una piedra!
- Marc Franq. subió a mi automóvil y quiso robar las llaves de mi auto e intenté salir de mi
auto tanto como pude.
- Él usó todos los medios físicos para mantenerme en La-Porkera, en mi auto y fuera de
mi auto.
- Amenazó con matarme y enterrarme en el bosque. También amenazó a mi familia más
tarde &quot;Vendremos a tu casa y te echaremos&quot;
- Más tarde &quot;Héctor Taus&quot; llegó y comenzó a demoler mi automóvil. Abrimos 2 denuncias
diferentes contra Héctor por vandalismo y violencia física y amenazas con la intención de
matarme.
- Marc Franquesa me debe unos cuantos cientos de euros por materiales, los guarda
(más precisamente: ¡los robó!) en La Porkera y lo único que quiero es recuperarlos, pero
lo niegoó y rechazó todo acceso o compensación por los materiales almacenados o
destruidos.
Más evidencia:
- El propietario &#39;Antoni Vilardell&#39; de La-Porkera estaba allí en ese momento. Él puede ser
testigo de la violencia brutal en contra de mi. Estuve gritando por ayuda durante unos 30
minutos.
- &#39;David Verges&#39;, &#39;Anne Barbier&#39; pueden ser testigos de mis múltiples lesiones causadas
por Marc F.
Otro:
Traté de denunciar a Marc Franquesa por demoler mi auto, mantenerme en contra de mi
voluntad y también múltiples amenazas de matarme o dañar a mi familia. La policía se
negó a tomar la denuncia.
Daños y perjuicios:
- daños múltiples en mi coche = 650 euros

- lesiones múltiples, incapaz de trabajar durante 10 días
- tarifas para equipo de seguridad adicional = 800 euros

-------------------------------------------------- English - Orginial

I hereby appeal the sentence "50/2017 T" - 1/12/2017 for the following reasons :

- 'Marc Franquesa Aran' kept me against my will for 45 minutes long at La-Porkera for no reason. As far I understood, he kept me against my will at La-Porkera in order to wait for Hector-Taus who repeatedly destroyed or stole my materials/property. Hector also physically did beat me up before. I had no choice to flee the scene at any cost. I acted out of self defense!
- I didn't harm Franq with a stone!
- Marc Franq. came into my car and wanted to steal my car keys and I tried to push him out of my car as much I could.
- He used all physical means to keep me at La-Porkera, in my car and outside of my car.
- He threatened to kill me and burrie me in the forest. He also threatened my family later on "We come to your house and kick you out".
- Later "Hector Taus" arrived and started to demolish my car. We opened 2 different denounces against Hector for multiple vandalism and physical violence and threats with the intention to kill me.
- Marc Franq. owes me a few hundred euros for materials from me, he keeps (more precisely : steals!) them in La-Porkera and I all wanted is to have it back but denied and refused me any access or compensation for the kept or destroyed materials.

More evidence: 

- The owner 'Antoni Vilardell' of La-Porkera was there at the time. He can witness the brutal violence against me. I was screaming for help for around 30 minutes long. 
- 'David Verges', 'Anne Barbier' can witness my multiple injuries caused by Marc F.

Other:

I tried to denounce Marc Franq. for demolishing my car, keeping me against my will and also multiple threats me to kill me or harm my family. The police refused to take the denounce.  

Damages: 

- multiple damages at my car = 650 Euro
- multiple injuries, incapable to work for 10 days
- fees to additional security equipment = 800 Euro

Total damages caused by Marc Franq. & Hector Taus

vandalism, material theft: 3430  + (ca. 200 Euro in yet not returned materials/tools ) = 3630

Total indirect damages (revenue loss due to times visiting lawyer or police )  : 2520 Euro

Total  : 6350 Euro

(Defamation damages not included)

My -lost - contributions to the Project:

1400 Euro in materials/supplies
200 Euro in cash quotes
6,5 months of full time work

Read the full story : http://pearls-media.com/control-freak/2017/11/13/the-fight-with-the-fake-punks-squatting-the-la-porkera-project2/



