The *Control Freak* SDK provides a general purpose API dealing with times and dates. This API is implemented
by <a href="https://http://momentjs.com/docs">Momentjs</a>
