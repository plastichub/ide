---
layout: docMain
---

## Introduction

* [How to load deliteful custom elements & modules](Widgets/setup)
* [Introduction to styling deliteful custom element widgets](Widgets/styling)

## Basic Widgets

* [Button](Widgets/Button)
* [Checkbox](Widgets/Checkbox)
* [Combobox](Widgets/Combobox)
* [ProgressBar](Widgets/ProgressBar)
* [ProgressIndicator](Widgets/ProgressIndicator)
* [RadioButton](Widgets/RadioButton)
* [Select](Widgets/Select)
* [Slider](Widgets/Slider)
* [StarRating](Widgets/StarRating)
* [Switch](Widgets/Switch)
* [Toaster](Widgets/Toaster)
* [ToggleButton](Widgets/ToggleButton)

## List

* [List](Widgets/list/List)
* [PageableList](Widgets/list/PageableList)

## Containers & Layouts

* [LinearLayout](Widgets/LinearLayout)
* [ScrollableContainer](Widgets/ScrollableContainer)
* [SidePane](Widgets/SidePane)
* [ViewStack](Widgets/ViewStack)
* [SwapView](Widgets/SwapView)
* [ResponsiveColumns](Widgets/ResponsiveColumns)

## Other Widgets

* [ViewIndicator](Widgets/ViewIndicator)

## Non Visual Custom Elements

* [Store](Widgets/Store)

## Other Modules

* [features](Widgets/features)
* [channelBreakpoints](Widgets/channelBreakpoints)
