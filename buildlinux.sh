#!/usr/bin/env bash
cd ./Code/client/src
sh buildxcf.sh
cd ../../../

cd server/nodejs
gitus
grunt platform
gitcs
cd ../..

sh prepare.sh
cd build/electron-template
grunt platform -dist=true
#grunt platform --package=true -dist=true
#cp releasesLinux/Control-Freak_amd64.deb ../../dist/linux/Control-Freak_amd64.deb
cd ../../dist/linux

git add -A .

gitc "update"
