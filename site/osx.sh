#!/usr/bin/env bash
rm -rf osx-dist
git clone https://gitlab.com/xamiro/osx-dist-64.git osx-dist
cd osx-dist
set -x
#rm -rf ../htdocs/wp-content/uploads/downloads/net-commander-os.dmg
rm -rf ../../htdocs/control-freak/wp-content/uploads/downloads/control-freak_osx.zip
#cp Control-Freak_1.0.0.dmg ../../htdocs/wp-content/uploads/downloads/net-commander-osx.dmg
zip -9 -q -r ../../htdocs/control-freak/wp-content/uploads/downloads/control-freak-osx.zip ./*
#/[ddownload id="3581"]
#http://net-commander.com/?ddownload=3581
