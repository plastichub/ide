#!/usr/bin/env bash

cd server/nodejs
#grunt platform
cd ../../

#grunt build-platform-server --stop=false --platform=windows --buildModules=false
#grunt build-platform-server --stop=false --platform=windows

if [ ! -d server/nodejs/dist/windows/node_modules ]; then
    echo "Cant find node modules in windows ----- incomplete build !!!!"
    exit;
fi

##just to get server/nodejs/osx_64
#grunt dist-prepare
#grunt update-dist --buildClient=false --platforms=windows --commit=true
#grunt build-platform-electron --platform=windows --stop=false
#calls after: grunt update-dist --buildClient=false --platforms=windows
mv dist/windows/.git dist/gitw
grunt build-platform-installer --platform=windows --stop=false
mv dist/gitw dist/windows/.git
grunt deploy-installer --platform=windows --stop=false
cd dist/windows
git lfs track ./Control-Freak.exe
git add -A .
gitc "update"
cd ../..
grunt deploy-zip --platform=windows --stop=false

# grunt update-dist --buildClient=false --platforms=windows --php=false --commit=true
# grunt deploy-zip --platform=windows