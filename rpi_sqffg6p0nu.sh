#!/bin/bash
# myDevices setup script

NAME="$0"
echo $NAME
codeRaw="${NAME##*_}"
inviteCode="${codeRaw%.*}"
cd /tmp
wget -O /tmp/myDevices-mds.tar.gz "http://updates.mydevices.com/raspberry/myDevices-1.0.tar.gz"
tar -xzvf /tmp/myDevices-mds.tar.gz 
cd /tmp/myDevices-1.0
chmod +x /tmp/myDevices-1.0/setup.sh
/tmp/myDevices-1.0/setup.sh -code "$inviteCode" "$@"
rm -rf /tmp/myDevices-1.0
rm -rf /tmp/myDevices-mds.tar.gz
