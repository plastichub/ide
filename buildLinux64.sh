#!/usr/bin/env bash

#grunt clean:dist-linux_64

grunt build-platform-server --stop=false --buildModules=true --platform=linux_64

if [ ! -d server/nodejs/dist/linux_64/node_modules ]; then
    echo "Cant find node modules in linux_64 ----- incomplete build !!!!"
    #exit;
fi
##just to get server/nodejs/osx_64
#grunt dist-prepare

grunt update-dist --platforms=linux_64 --commit=true

grunt build-platform-electron  --stop=false --platform=linux_64

grunt build-platform-installer --stop=false --platform=linux_64

grunt deploy-installer --stop=false --platform=linux_64

cd dist/linux_64
git lfs track ./Control-Freak
git add -A .
gitc "update"
cd ../..
#grunt deploy-zip --stop=false --platform=linux_64
