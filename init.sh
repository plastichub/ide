#!/usr/bin/env bash
git submodule update --init
git submodule foreach "git checkout master"
git submodule foreach "git pull"
npm install
cd Code/client
sh init.sh
cd ..
cd utils
sh init.sh
cd ..
cd ..
cd build/electron-template
npm install