#!/usr/bin/env bash
BASEDIR=$(cd $(dirname $0) && pwd)/
cd $BASEDIR
npm build
set -x
/usr/local/lib/node_modules/electron-prebuilt/dist/electron --file="$1" --editor="$2" ./build