/* jshint node:true */

module.exports = function (grunt) {

    var DIST_PLATFORM = grunt.option('DIST_PLATFORM');
    var OS = grunt.option('OS');
    var path = require('path');
    var net = require('net');
    var os = require('os');
    var _child = require('child_process');
    var _ = require('lodash');
    var request = require('request');
    var platforms = ['windows', 'linux_32', 'linux_64', 'osx'];
    var _platforms = grunt.option('platforms');
    var tcpPortUsed = require('tcp-port-used');
    var DIST_PLATFORMS_PLATFORMS = ['windows', 'linux_32', 'linux_64', 'osx', 'web'];
    if (_platforms) {
        if (_platforms.indexOf(',') !== -1) {
            DIST_PLATFORMS_PLATFORMS = _platforms.split(',');
        } else {
            DIST_PLATFORMS_PLATFORMS = [_platforms];
        }
    }
    var os = require('os');
    var arch = os.arch();
    var is32 = arch !== 'x64';
    var is64 = arch === 'x64';
    var DIST_NAME = 'Linux';
    var CF_ROOT = path.resolve('../../');
    var platform = os.platform();
    if (platform === 'win32') {
        DIST_NAME = 'windows';
    } else if (platform === 'darwin') {
        DIST_NAME = 'osx';
    } else if (platform === 'linux' && os.arch() == 'arm') {
        DIST_NAME = 'arm';
    } else if (platform === 'linux') {
        if (is32) {
            DIST_NAME = "linux_32";
        } else if (is64) {
            DIST_NAME = "linux_64";
        }
    }

    var DIST_MAPPINGS = {
        'osx': '/Control-Freak/Resources/',
        'windows': '',
        'arm': '',
        'linux_32': '',
        'linux_64': ''
    };

    var NODE_JS_ROOT = path.resolve('server/nodejs/dist') + "/";

    var PLATFORM_VMS = {
        windows: {
            host: 'windows',
            vmx: '/windows10/Windows 10 x64.vmx'
        },
        osx: {
            host: 'osx',
            vmx: '/mnt/anne/vmware/osx/OS X El Capitan.vmx'
        },
        linux_32: {
            host: 'linux_32',
            vmx: '/mnt/anne/vmware/ubuntu_32/Ubuntu/Ubuntu.vmx'
        }
    };

    var Promise = require('grunt-promise').using('bluebird');

    var PLATFORM_SSH = {
        windows: {
            buildElectron: 'build.bat',
            buildServer: 'buildServer.bat',
            buildInstaller: 'buildInstaller.bat',
            options: {
                host: 'windows',
                username: 'mc007',
                password: 'asdasd',
                ignoreErrors: true,
                suppressRemoteErrors: true
            }
        },
        linux_64: {
            buildElectron: 'cd /PMaster/projects/x4mm/build/electron-template; grunt platform --platform=linux_64',
            buildServer: 'source /etc/profile;cd /PMaster/projects/x4mm/server/nodejs/dist/linux_64; rm -rf node_modules;npm install > /dev/null 2>&1;cd ../../;grunt clean-platform --platform=linux_64',
            buildInstaller: 'cd /PMaster/projects/x4mm/; grunt update-dist --platforms=linux_64  --commit=false ; cp -rf ./dist/linux_64/* ./build/electron-template/tmplinux_64/Control-Freak/opt/Control-Freak/ ; cd /PMaster/projects/x4mm/build/electron-template;grunt createInstaller --platform=linux_64',
            options: {
                host: 'localhost',
                username: 'mc007',
                password: '213,,asd',
                ignoreErrors: true,
                suppressRemoteErrors: true
            }
        },
        linux_32: {
            buildElectron: 'cd /PMaster/projects/x4mm/build/electron-template; grunt platform -v --platform=linux_32 ',
            buildServer: 'cd /PMaster/projects/x4mm/server/nodejs/dist/linux_32; npm3 install > /dev/null 2>&1',
            buildInstaller: 'cd /mnt/hgfs/PMaster/projects/x4mm/; cd /mnt/hgfs/PMaster/projects/x4mm/build/electron-template; grunt createInstaller --platform=linux_32',
            options: {
                host: 'linux_32',
                username: 'mc007',
                password: 'asdasd',
                ignoreErrors: true,
                suppressRemoteErrors: true
            }
        },
        osx: {
            buildElectron: 'sh build.sh',
            buildServer: 'sh buildServer.sh',
            buildInstaller: 'sh buildInstaller.sh',
            wait: 60,
            options: {
                host: 'osx',
                username: 'admin',
                password: '123',
                ignoreErrors: true,
                suppressRemoteErrors: true
            }
        },
        arm: {
            buildElectron: 'sh build.sh',
            buildServer: 'source /etc/profile;cd /install/xDojoWatch;git reset --hard origin;gitus;cd dist/arm; rm -rf node_modules; npm3 install > /dev/null 2>&1;cd ../../;grunt clean-platform --platform=arm;gitc',
            buildInstaller: 'sh buildInstaller.sh',
            wait: 60,
            options: {
                host: 'pi',
                username: 'pi',
                password: 'asdasd',
                ignoreErrors: true,
                suppressRemoteErrors: true
            }
        }
    };
    var ELECTRON_BEFORE = {
        windows: {
            command: "grunt clean:dist-windows"
        },
        linux_32: {
            command: "rm -rf build/electron-template/tmplinux_32"
        },
        linux_64: {
            command: "rm -rf build/electron-template/tmplinux_64"
        },
        osx: {
            command: "rm -rf dist/osx/*"
        }
    };
    var ELECTRON_AFTER = {
        windows: {
            command: ""
        },
        linux_32: {
            command: "grunt update-dist --buildClient=false --platforms=linux_32 --php=false --commit=false; cp -rf ./build/electron-template/tmplinux_32/Control-Freak/opt/Control-Freak/* ./dist/linux_32/"
        },
        linux_64: {
            command: "grunt clean:dist-linux_64; cp -rf ./build/electron-template/tmplinux_64/Control-Freak/opt/Control-Freak/* ./dist/linux_64/"
        },
        osx: {
            command: "grunt update-dist --buildClient=false --platforms=osx --php=false --commit=false"
        }
    };
    var SERVER_BEFORE = {
        windows: {
            command: "cd server/nodejs/dist/windows; rm -rf node_modules"
        },
        osx: {
            command: "cd server/nodejs/dist/osx_64; rm -rf node_modules"
        },
        linux_32: {
            command: "cd server/nodejs/dist/linux_32; rm -rf node_modules"
        },
        linux_64: {
            command: "cd server/nodejs/dist/linux_64; rm -rf node_modules"
        }
    };
    var SERVER_AFTER = {
        windows: {
            command: [
                "cd server/nodejs; grunt clean-platform --platform=windows",
                "cd dist/windows ",
                "git add -A . > /dev/null 2>&1; gitc > /dev/null 2>&1 ",
                "rm -rf ../../../../dist/windows/server/windows/* ",
                "mkdir -p ../../../../dist/windows/server/windows ",
                "cp -rf ./ ../../../../dist/windows/server/windows/"
            ].join(' ; ')
        },
        osx: {
            command: [
                "cd server/nodejs; grunt clean-platform --platform=osx_64;cd dist/osx_64",
                "git add -A . > /dev/null 2>&1; gitc > /dev/null 2>&1",
                "rm -rf ../../../../dist/all/server/osx_64/*",
                "cp -rf ./ ../../../../dist/all/server/osx_64/"].join(' ; ')
        },
        linux_64: {
            command: [
                "cd /PMaster/projects/x4mm/server/nodejs/dist/linux_64",
                "git add -A . > /dev/null 2>&1; gitc > /dev/null 2>&1"
            ].join(' ; ')
        },
        linux_32: {
            command: [
                "cd /PMaster/projects/x4mm/server/nodejs/; grunt clean-platform --platform=linux_32 ",
                "cd dist/linux_32 ; git add -A . > /dev/null 2>&1; gitc > /dev/null 2>&1 "].join(' ; ')
        },
        arm: {
            command: ""
        }
    };
    var INSTALLER_AFTER = {
        windows: {
            command: ""
        },
        osx: {
            command: "sh commitOSX.sh"
        }
    };

    //console.log('update platforms : ' + DIST_PLATFORMS_PLATFORMS.join('  '));
    var DIST_ROOT = path.resolve(CF_ROOT + '/dist/');
    var DIST_ALL = path.resolve(CF_ROOT + '/dist/all');
    var DIST_PLATFORM = path.resolve(DIST_ROOT + '/' + DIST_NAME);

    //////////////////////////////////////////////////////////
    //
    //
    //
    var jetpack = require('fs-jetpack');
    var Q = require('q');
    var rollup = require('rollup');
    var grequire = require('requireg');
    var asar = require('asar');

    var utils = require(path.resolve('./tasks/utils'));
    var projectDir = jetpack;
    var srcDir = projectDir.cwd('./app');
    var destDir = projectDir.cwd('./build');
    var childProcess = require('child_process');

    var isWin32 = process.platform === 'win32';
    //electronGlobal points to /usr/local/lib/node_modules/electron/dist/electron , the exe
    var electronGlobal = path.resolve(grequire('electron') + './..');


    var paths = {
        copyFromAppDir: [
            './node_modules/**',
            './vendor/**',
            './**/*.html',
            './**/*.css',
            './**/*.+(jpg|png|svg)'
        ]
    };
    var bundle = function (src, dest) {
        var deferred = Q.defer();
        rollup.rollup({
            entry: src
        }).then(function (bundle) {
            var jsFile = path.basename(dest);

            var result = bundle.generate({
                format: 'cjs',
                sourceMap: false,
                sourceMapFile: jsFile
            });

            // Wrap code in self invoking function so the variables don't
            // pollute the global namespace.
            var isolatedCode = '(function () {' + result.code + '\n}());';
            return Q.all([
                destDir.writeAsync(dest, isolatedCode + '\n//# sourceMappingURL=' + jsFile + '.map')
            ]);
        }).then(function () {
            deferred.resolve();
        }).catch(function (err) {
            console.error('Build: Error during rollup', err.stack);
        });

        return deferred.promise;
    };
    //////////////////////////////////////////////////////////////
    //
    //  Prepare basics
    //
    //
    grunt.registerTask('clean-electron-app', 'clean ./build folder', function () {
        destDir.dir('.', {
            empty: true
        });
    });
    grunt.registerTask('copy-electron-app', 'copy from ./app to ./build', function () {
        var done = this.async();
        projectDir.copyAsync('app', destDir.path(), {
            overwrite: true,
            matching: paths.copyFromAppDir
        }).then(function () {
            console.log('copy-electron-app : done');
            done();
        });
    });
    grunt.registerTask('build-electron-app', 'compile ./app/background.js to ./build/background', function () {
        var done = this.async();
        bundle(srcDir.path('background.js'), destDir.path('background.js')).then(function () {
            done();
        });
    });
    grunt.registerTask('post-electron-app', 'adjust ./app/package.json template to ./build/package.json', function () {
        var manifest = srcDir.read('package.json', 'json');

        var env_name = "production";
        // Add "dev" or "test" suffix to name, so Electron will write all data
        // like cookies and localStorage in separate places for each environment.
        switch (env_name) {
            case 'development':
                manifest.name += '-dev';
                manifest.productName += ' Dev';
                break;
            case 'test':
                manifest.name += '-test';
                manifest.productName += ' Test';
                break;
        }
        destDir.write('package.json', manifest);
        // Copy environment variables to package.json file for easy use
        // in the running application. This is not official way of doing
        // things, but also isn't prohibited ;)
        manifest.env = projectDir.read('config/env_' + env_name + '.json', 'json');

        destDir.write('package.json', manifest);


    });
    grunt.registerTask('copy-electron-run-time', 'copy over electrons binaries', function () {

        var manifest = jetpack.read('app/package.json', 'json');

        if (platform === 'darwin') {
            electronGlobal = electronGlobal.replace('/Contents/MacOS', '');
        }

        var dst = DIST_PLATFORM;
        if (platform === 'darwin') {
            dst = DIST_PLATFORM + '/' + manifest.name + '.app';
        }

        //console.log('copy ########' + electronGlobal + ' to ' + dst);

        projectDir.copy(electronGlobal, dst, {
            overwrite: true
        });
        var readyAppDir = jetpack.cwd(DIST_PLATFORM);
        //console.log('rename ' + readyAppDir.path('electron.exe') + ' to ' + manifest.name + ' @ ' + platform);

        if (platform === 'win32') {
            readyAppDir.rename("electron.exe", manifest.name + '.exe');
        } else if (platform === 'darwin') {

        }
        else if (platform === 'linux') {
            readyAppDir.rename("electron", manifest.name);
        }
        var done = this.async();
        //console.log('create asar: ' + jetpack.cwd('build').path() + ' to ' + jetpack.cwd(dst).path('Contents/Resources/app.asar'));
        //console.log('readyAppDir : ' + readyAppDir.path());
        if (platform === 'darwin') {
            var insideApp = jetpack.cwd(dst);
            asar.createPackage(jetpack.path('build'), jetpack.cwd(dst).path('Contents/Resources/app.asar'), function () {
                console.log('done asar!');
                var info = jetpack.read('resources/osx/Info.plist');
                info = utils.replace(info, {
                    productName: manifest.productName,
                    identifier: manifest.identifier,
                    version: manifest.version,
                    copyright: manifest.copyright,
                    build: '1.1.0'
                });
                //insideApp.write('Contents/Info.plist', info);
                console.log('wrote Info.plist', info);
                //insideApp.remove('Contents/Resources/default_app');
                //insideApp.remove('Contents/Resources/atom.icns');
                done();
            });

            //return;

            ////////////////////////////////////////////////////////
            //
            /*
            var info = jetpack.read('resources/osx/Info.plist');
            info = utils.replace(info, {
                productName: manifest.productName,
                identifier: manifest.identifier,
                version: manifest.version,
                copyright: manifest.copyright,
                build: '1.1.0'
            });
            */

            //insideApp.write('Contents/Info.plist', info);

            //console.log('write plist to '+jetpack.cwd(dst).path());



            // Prepare Info.plist of Helper apps
            /*
            [' EH', ' NP', ''].forEach(function (helper_suffix) {
                info = jetpack.read('resources/osx/helper_apps/Info' + helper_suffix + '.plist');
                info = utils.replace(info, {
                    productName: manifest.productName,
                    identifier: manifest.identifier
                });
                insideApp.write('Contents/Frameworks/Electron Helper' + helper_suffix + '.app/Contents/Info.plist', info);
            });*/



            //insideApp.remove('Contents/Resources/default_app');
            //insideApp.remove('Contents/Resources/atom.icns');
        } else {
            asar.createPackage(jetpack.cwd('build').path(), readyAppDir.path('resources/app.asar'), function () {
                done();
            });
        }

    });
    grunt.registerTask('bundle-electron-app', 'Prepare main task', function () {
        grunt.task.run('clean-electron-app');
        grunt.task.run('copy-electron-app');
        grunt.task.run('build-electron-app');
        grunt.task.run('post-electron-app');
        grunt.task.run('copy-electron-run-time');
    });
    //////////////////////////////////////////////////////////////
    //
    //
    grunt.registerTask('create-electron-installer_linux', 'Linux: create the deb installer', function () {
        var manifest = jetpack.read('app/package.json', 'json');
        var packName = manifest.name;// + '_' + manifest.version;

        var DIST_LINKED = './tmp' + DIST_NAME + '/' + packName + '/opt/' + packName;

        if (jetpack.exists(DIST_LINKED)) {
            childProcess.execSync('sudo umount ' + path.resolve(DIST_LINKED) + ' || true');
            jetpack.remove(path.resolve(DIST_LINKED));
            jetpack.remove('./tmp' + DIST_NAME);
        }
        /////////////////////////////////////////////////////////////
        //
        //  Prepare layout
        //

        //  ./tmplinux_64
        jetpack.dir('./tmp' + DIST_NAME, { empty: true });


        //  ./tmplinux_64/opt
        var TMP_DIR = jetpack.dir('./tmp' + DIST_NAME + '/' + packName);
        jetpack.dir('./tmp' + DIST_NAME + '/' + packName + '/opt/');

        //create sym link from ../../dist/platform to tmp

        //jetpack.symlink(DIST_PLATFORM,DIST_LINKED);
        DIST_LINKED = jetpack.dir(DIST_LINKED);


        childProcess.execSync('sudo mount -o bind ' + DIST_PLATFORM + ' ' + DIST_LINKED.path());
        var desktop = jetpack.read('resources/linux/app.desktop');
        desktop = utils.replace(desktop, {
            name: manifest.name,
            productName: manifest.productName,
            description: manifest.description,
            version: manifest.version,
            author: manifest.author
        });
        TMP_DIR.write('usr/share/applications/' + manifest.name + '.desktop', desktop);

        var server = jetpack.read('resources/linux/server.desktop');
        server = utils.replace(server, {
            name: manifest.name,
            productName: manifest.productName,
            description: manifest.description,
            version: manifest.version,
            author: manifest.author
        });

        TMP_DIR.write('usr/share/applications/' + manifest.name + '-server.desktop', server);

        jetpack.copy('resources/icon.png', DIST_PLATFORM + '/icon.png', { overwrite: true });

        ////////////////////////////////////////////////////////////
        //
        //  Installer
        //
        //grunt.util.log('Creating DEB package...');


        // Counting size of the app in KiB
        var appSize = Math.round(DIST_LINKED.inspectTree('.').size / 1024);

        // Preparing debian control file
        var control = jetpack.read('resources/linux/DEBIAN/control');
        control = utils.replace(control, {
            name: manifest.name,
            description: manifest.description,
            version: manifest.version,
            author: manifest.author,
            size: appSize,
            arch: is64 ? 'amd64' : 'i386'
        });
        TMP_DIR.write('DEBIAN/control', control);

        var postinst = jetpack.read('resources/linux/DEBIAN/postinst');

        TMP_DIR.write('DEBIAN/postinst', postinst);

        childProcess.exec('chmod 755 ' + TMP_DIR.path() + '/DEBIAN/postinst');

        var done = this.async();

        var debFileName = packName + '_' + DIST_NAME + '.deb';
        var releasesDir = jetpack.dir(path.resolve(CF_ROOT + '/dist/installer'));
        var debPath = releasesDir.path(debFileName);
        // Build the package...
        childProcess.exec('fakeroot dpkg-deb -Zxz --build ' + TMP_DIR.path().replace(/\s/g, '\\ ') + ' ' + debPath.replace(/\s/g, '\\ '),
            function (error, stdout, stderr) {
                if (error || stderr) {
                    console.log("ERROR while building DEB package:");
                    console.log(error);
                    console.log(stderr);
                } else {
                    console.log('DEB package ready!', debPath);
                }

                if (jetpack.exists(DIST_LINKED.path())) {
                    childProcess.execSync('sudo umount ' + DIST_LINKED.path());
                    childProcess.execSync(jetpack.remove(DIST_LINKED.path()));
                }
                done();
            });
    });


    grunt.registerTask('build-electron-app_linux', 'Linux: copies over electron`s binaries and adjust', function () {
        grunt.task.run('create-electron-installer_' + utils.os());
    });

    grunt.registerTask('create-electron-app', 'Copy over electrons binaries', function () {
        grunt.task.run('bundle-electron-app');
        //grunt.task.run('build-electron-app_'+utils.os());
    });
    grunt.registerTask('create-electron-installer', 'Copy over electrons binaries', function () {
        grunt.task.run('bundle-electron-app');
        grunt.task.run('build-electron-app_' + utils.os());
    });

};
