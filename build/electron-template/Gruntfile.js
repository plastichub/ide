/*global module */
module.exports = function (grunt) {
    function log(err, stdout, stderr, cb) {
        console.log(arguments);
        cb();
    }
    var path = require('path');
    var os = require('os');
    var os_suffix = 'Linux';
    var ROOT = path.resolve('../../');
    var extraArgsGulp = process.argv.slice(3).join(' ');
    var arch = os.arch();

    var is32 = arch !== 'x64';
    var is64 = arch === 'x64';

    var platform = os.platform();
    if (platform === 'win32') {
        os_suffix = 'windows';
    } else if (platform === 'darwin') {
        os_suffix = 'osx';
    } else if (platform === 'linux' && os.arch() == 'arm') {
        os_suffix = 'arm';
    } else if (platform === 'linux') {
        if (is32) {
            os_suffix = "linux_32";
        } else if (is64) {
            os_suffix = "linux_64";
        }
    }
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //
    //  OS specific help variables
    //
    var OS = os_suffix.toLowerCase();
    var OS_EXE_SUFFIX = OS === 'windows' ? '.exe' : '';
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //
    //  Dist paths
    //
    var DIST_DIRECTORY = path.resolve('./tmp' + os_suffix + '/Control-Freak') + path.sep;//windows default
    if (platform === 'linux' || platform === 'arm') {
        DIST_DIRECTORY = path.resolve('./tmp' + os_suffix + '/Control-Freak/opt/Control-Freak') + '/';
    } else if (OS === 'osx') {
        DIST_DIRECTORY = path.resolve(ROOT + '/dist/' + OS + '/Control-Freak.app/Resources') + '/';
    }

    var DIST_FINAL = path.resolve(ROOT + '/dist/' + OS) + path.sep;//windows default
    //console.error('DIST_FINAL : ' +DIST_FINAL);
    var SERVER_TEMPLATES = path.resolve('./../../server-template');
    // Server paths
    var NGINX_DIST = path.resolve(SERVER_TEMPLATES + '/nginx') + '/';
    var PHP_DIST = path.resolve(SERVER_TEMPLATES + '/php/php-dist-' + OS) + '/';
    var MONGO_DIST = path.resolve(SERVER_TEMPLATES + '/mongo') + '/';
    var DEVICE_SERVER_DIST = path.resolve('../../server/nodejs/dist/' + OS);

    //console.error('DEVICE_SERVER_DIST : ' +DEVICE_SERVER_DIST);
    var DIST_ALL = path.resolve(ROOT + '/dist/all');
    console.log('platform : ' + platform);
    console.log('os suffix: ' + os_suffix);
    console.log('OS ' + OS);
    /**
     * on windows :
     *  OS : windows
     *  platform: win32
     *  os suffix : windows
     *
     * on linux - x64 :
     *  OS : linux_64
     *  platform: linux
     *  os suffix : linux_64
     *
     */

    /*
    console.error('OS : ' +OS);
    console.error('platform : ' +platform);
    console.error('os suffix : ' +os_suffix);
    return;
    */

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON("package.json"),
        shell: {
            electron: {
                command: [
                    'gulp release --env=production ' + extraArgsGulp
                ].join('&&'),
                options: {
                    stderr: true
                }
            },
            "electron-update": {
                command: [
                    'gulp release --env=production --clean=false ' + extraArgsGulp
                ].join('&&'),
                options: {
                    stderr: true
                }
            },
            "electron-installer": {
                command: [
                    'gulp release --env=production --package=true' + extraArgsGulp
                ].join('&&'),
                options: {
                    stderr: true
                }
            }
        },
        copy: {
            "dist-misc": {
                src: [
                    '**'
                ],
                dest: DIST_DIRECTORY,
                expand: true,
                flatten: false,
                cwd: ROOT + '/dist/misc'
            },
            "dist-final": {
                src: [
                    '**'
                ],
                dest: DIST_FINAL,
                expand: true,
                flatten: false,
                cwd: DIST_DIRECTORY
            }
        },
        clean: {
            options: {
                force: true
            },
            'dist-final': [
                DIST_FINAL + '/**',
                "!" + DIST_FINAL + '/README.md',
                "!" + DIST_FINAL + '/.git'
            ]
        },
        chmod: {
            options: {
                mode: '775'
            },
            'xcf-server': {
                // Target-specific file/dir lists and/or options go here.
                src: [DIST_DIRECTORY + '/server/nodejs/app/xide/server' + OS_EXE_SUFFIX]
            },
            'mongo': {
                // Target-specific file/dir lists and/or options go here.
                src: [DIST_DIRECTORY + '/mongo/mongod']
            },
            'php': {
                // Target-specific file/dir lists and/or options go here.
                src: [DIST_DIRECTORY + '/php/php-cgi']
            },
            'nginx': {
                // Target-specific file/dir lists and/or options go here.
                src: [DIST_DIRECTORY + '/nginx']
            },
            'scripts': {
                options: {
                    mode: '777'
                },
                src: [DIST_DIRECTORY + '/start_linux.sh']
            },
            'dist': {
                options: {
                    mode: '777'
                },
                // Target-specific file/dir lists and/or options go here.
                src: [
                    DIST_DIRECTORY
                ]

            },
            'xide': {
                options: {
                    mode: '777'
                },
                // Target-specific file/dir lists and/or options go here.
                src: [
                    DIST_DIRECTORY + 'server/nodejs/server'
                ]

            },
            'nginx-misc': {
                options: {
                    mode: '777'
                },
                // Target-specific file/dir lists and/or options go here.
                src: [
                    DIST_DIRECTORY + '/temp/**',
                    DIST_DIRECTORY + '/error.log',
                    DIST_DIRECTORY + '/access.log',
                    DIST_DIRECTORY + '/data/**'
                ]
            },
            'linux-postinst': {
                src: [
                    './tmpLinux/Control-Freak/DEBIAN/postinst'
                ]
            }
        },
        intern: {
            options: {
                runType: 'runner',
                config: './tests/intern',
                reporters: ['Runner']
            },
            remote: {},
            local: {
                options: {
                    config: './tests/intern'
                }
            },
            node: {
                options: {
                    runType: 'client',
                    reporters: [
                        "xide/Console",
                        { id: 'JUnit', filename: 'report.xml' }
                    ]
                }
            },
            proxy: {
                options: {
                    proxyOnly: true
                }
            }
        },
        sshexec: {
            "cp-dist-all-osx": {
                command: [
                    'sh /PMaster/projects/x4mm/build/electron-template/cp-dist-all-osx.sh'
                ],
                options: {
                    host: 'mc007',
                    username: 'mc007',
                    password: '213,,asd'
                }
            },
            "cp-nodejs-osx": {
                command: [
                    'sh /PMaster/projects/x4mm/build/electron-template/cp-nodejs-osx.sh'
                ],
                options: {
                    host: 'mc007',
                    username: 'mc007',
                    password: '213,,asd'
                }
            },
            "cp-nodejs-windows": {
                command: [
                    'sh /PMaster/projects/x4mm/build/electron-template/cp-nodejs-windows.sh'
                ],
                options: {
                    host: 'mc007',
                    username: 'mc007',
                    password: '213,,asd'
                }
            },
            "cp-dist-all-windows": {
                command: [
                    'sh /PMaster/projects/x4mm/build/electron-template/cp-dist-all-windows.sh'
                ],
                options: {
                    host: 'mc007',
                    username: 'mc007',
                    password: '213,,asd'
                }
            },
            "cp-dist-all-linux_32": {
                command: [
                    'sh /PMaster/projects/x4mm/build/electron-template/cp-dist-all-linux_32.sh'
                ],
                options: {
                    host: 'mc007',
                    username: 'mc007',
                    password: '213,,asd'
                }
            }
        }

    });
    grunt.loadNpmTasks('grunt-shell');
    grunt.loadNpmTasks("grunt-contrib-copy");
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks("grunt-extend-config");
    grunt.loadNpmTasks('grunt-contrib-rename');
    grunt.loadNpmTasks('grunt-chmod');
    grunt.loadNpmTasks('intern');
    grunt.loadNpmTasks('grunt-ssh');
    grunt.loadTasks('gtasks');

    // Aliases
    grunt.registerTask('dist-windows', [
        'copy:dist-windows-mongo',
        'copy:dist-windows-utils',
        'copy:dist-windows-code'
    ]);
    grunt.registerTask('dist-linux', [
        "copy:dist-linux-code",
        "copy:dist-linux-nginx",
        "copy:dist-linux-server"
    ]);
    grunt.registerTask('dist-osx', [
        "copy:dist-osx-code",
        "copy:dist-osx-servers",
        "copy:dist-osx-server"
    ]);
    grunt.registerTask('windows', [
        'shell:electron',
        'dist-windows'
    ]);
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //
    //  Code - Deployment (ROOT/dist/all)
    //
    grunt.extendConfig({
        copy: {
            "dist-all": {
                src: [
                    '**'
                ],
                dest: DIST_DIRECTORY,
                expand: true,
                flatten: false,
                cwd: DIST_ALL
            }
        }
    });
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //
    //  NGINX - Deployment
    //
    var NGINX_BINARY = path.resolve(NGINX_DIST + '/nginx-' + OS + OS_EXE_SUFFIX);
    grunt.extendConfig({
        copy: {
            'nginx-platform-dist': {
                src: NGINX_BINARY,
                expand: true,
                flatten: true,
                dest: DIST_DIRECTORY
            },
            'nginx-platform-misc': {
                src: ['**'],
                expand: true,
                flatten: false,
                dest: DIST_DIRECTORY,
                cwd: path.resolve(NGINX_DIST + '/all')
            }
        },
        rename: {
            'nginx-platform-rename': {
                files: [
                    {
                        src: DIST_DIRECTORY + 'nginx-' + OS + OS_EXE_SUFFIX,
                        dest: DIST_DIRECTORY + '/nginx' + OS_EXE_SUFFIX
                    }
                ]
            }
        }
    });
    grunt.registerTask('nginx-platform-all', 'Copies the pre-compiled NGINX binary and its misc files to platform dist', function () {
        //grunt.task.run('copy:nginx-platform-dist');
        //grunt.task.run('copy:nginx-platform-misc');
    });


    grunt.registerTask('dist-final', "", function () {
        if (OS == 'windows') {
            grunt.task.run('copy:dist-final');
        } else if (OS === 'linux') {


        } else if (OS === 'osx') {
            grunt.extendConfig({
                copy: {
                    'osx-dist': {
                        src: ['Control-Freak_1.0.0.dmg'],
                        expand: true,
                        flatten: false,
                        dest: DIST_FINAL,
                        cwd: './releasesOSX'
                    }
                }
            });
            grunt.task.run('copy:osx-dist');
        }
    });
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //
    //  Mongo - Deployment
    //
    /*
    var MONGO_BINARY = path.resolve(MONGO_DIST + '/mongod-' + OS + OS_EXE_SUFFIX);
    grunt.extendConfig({
        copy: {
            'mongo-platform-dist': {
                src: MONGO_BINARY,
                expand: true,
                flatten: true,
                dest: DIST_DIRECTORY + '/mongo/'
            }
        },
        rename: {
            'mongo-platform-rename': {
                files: [
                    {
                        src: path.resolve(DIST_DIRECTORY + '/mongo/mongod-' + OS + OS_EXE_SUFFIX),
                        dest: path.resolve(DIST_DIRECTORY + '/mongo/') + path.sep + 'mongod' + OS_EXE_SUFFIX
                    }
                ]
            }
        }
    });
    */
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //
    //  Aliases
    //
    grunt.registerTask('copy-platform-servers', 'Copies all server modules into platform dist', function () {
        //grunt.task.run('copy:mongo-platform-dist');
        if (OS === 'linux_64') {
            grunt.task.run('copy:dist-all');
        } else if (OS === 'osx') {

        } else if (platform === 'win32') {

        }
    });

    grunt.registerTask('createInstaller', 'Creates an installer, assumes that it runs on the host platform.', function () {



        var done = this.async();

        var platform = grunt.option('platform') || OS;

        console.log('-----------------------create installer ! ' + platform);
        var Q = require('q');
        var deferred = Q.defer();
        var jetpack = require('fs-jetpack');
        var asar = require('asar');
        var child_process = require('child_process');
        var path = require('path');
        var projectDir;
        var releasesDir;
        var tmpDir;
        var finalAppDir;
        var manifest;
        var utils = require('./tasks/utils');

        projectDir = jetpack;

        manifest = projectDir.read('app/package.json', 'json');

        releasesDir = projectDir.dir(path.resolve('../../dist/installer'));



        if (platform === 'osx') {
            tmpDir = projectDir.dir(path.resolve('../../dist/osx'), { empty: false });
            finalAppDir = tmpDir.cwd(manifest.productName + '.app');
            var appdmg = require('appdmg');
            var dmgName = manifest.name + '_' + manifest.version + '.dmg';
            // Prepare appdmg config
            var dmgManifest = projectDir.read('resources/osx/appdmg.json');
            dmgManifest = utils.replace(dmgManifest, {
                productName: manifest.productName,
                appPath: finalAppDir.path(),
                //appPath: 'Control-Freak.app',

                //basepath: path.resolve(tmpDir.path),
                dmgIcon: projectDir.path("resources/osx/dmg-icon.icns"),
                dmgBackground: projectDir.path("resources/osx/dmg-background.png")
            });

            tmpDir.write('appdmg.json', dmgManifest);

            console.log('wrote : manifest', dmgManifest);

            // Delete DMG file with this name if already exists
            releasesDir.remove(dmgName);

            console.log('Packaging to DMG file...from', tmpDir.path('appdmg.json'));

            var readyDmgPath = releasesDir.path(dmgName);

            appdmg({
                source: tmpDir.path('appdmg.json'),
                target: readyDmgPath
            }).on('error', function (err) {
                console.error(err);
            }).on('progress', function (res) {
                console.log('on app dmg progress', res);
            }).on('finish', function () {
                console.log('DMG file ready!', readyDmgPath);
                deferred.resolve();
            });

            //return deferred.promise;
        }

        if (platform === '_windows') {
            var finalPackageName = manifest.name + '_' + manifest.version + '.exe';
            var installScript = projectDir.read('resources/windows/installer.nsi');

            projectDir = jetpack;
            tmpDir = projectDir.dir(path.resolve('../../dist/windows'), { empty: false });

            installScript = utils.replace(installScript, {
                name: manifest.name,
                productName: manifest.productName,
                author: manifest.author,
                version: manifest.version,
                src: tmpDir.path(),
                dest: releasesDir.path(finalPackageName),
                icon: tmpDir.path('icon.ico'),
                setupIcon: projectDir.path('resources/windows/setup-icon.ico'),
                banner: projectDir.path('resources/windows/setup-banner.bmp')
            });

            tmpDir.write('installer.nsi', installScript);

            console.log('Building installer with NSIS...');

            // Remove destination file if already exists.
            releasesDir.remove(finalPackageName);


            // Note: NSIS have to be added to PATH (environment variables).
            var nsis = child_process.spawn('makensis', [
                tmpDir.path('installer.nsi')
            ], {
                    stdio: 'inherit'
                });

            nsis.on('error', function (err) {
                if (err.message === 'spawn makensis ENOENT') {
                    throw "Can't find NSIS. Are you sure you've installed it and"
                    + " added to PATH environment variable?";
                } else {
                    throw err;
                }
            });
            nsis.on('close', function () {

                console.log('Installer ready!', releasesDir.path(finalPackageName));
                done();
            });

        }

        if (platform === 'windows') {

            var finalPackageName = manifest.name + '_' + manifest.version + '.exe';
            var installScript = projectDir.read('resources/windows/installer2.nsi');

            projectDir = jetpack;

            tmpDir = projectDir.dir(path.resolve('../../dist/windows'), { empty: false });




            installScript = utils.replace(installScript, {
                name: manifest.name,
                productName: manifest.productName,
                author: manifest.author,
                version: manifest.version,
                src: tmpDir.path(),
                dest: releasesDir.path(finalPackageName),
                icon: tmpDir.path('icon.ico'),
                setupIcon: projectDir.path('resources/windows/setup-icon.ico'),
                INSTALL_MODE_PER_ALL_USERS: false,
                COMPANY_NAME: "comp",
                VERSION: "1.0",
                APP_ID: 'APPID',
                APP_GUID: 'APP_GUID',
                PROJECT_DIR: tmpDir.path(),
                BUILD_RESOURCES_DIR: 'C:\\',
                COMPRESS: "auto",
                MULTIUSER_INSTALLMODE_ALLOW_ELEVATION: false,
                banner: projectDir.path('resources/windows/setup-banner.bmp')
            });



            tmpDir.write('installer.nsi', installScript);

            console.log('Building installer with NSIS...');

            // Remove destination file if already exists.
            releasesDir.remove(finalPackageName);



            // Note: NSIS have to be added to PATH (environment variables).
            var nsis = child_process.spawn('makensis', [
                tmpDir.path('installer.nsi')
            ], {
                    stdio: 'inherit'
                });

            nsis.on('error', function (err) {
                console.log('nsis error : ', err);
                if (err.message === 'spawn makensis ENOENT') {
                    throw "Can't find NSIS. Are you sure you've installed it and"
                    + " added to PATH environment variable?";
                } else {
                    throw err;
                }
            });
            nsis.on('close', function () {
                console.log('nsis error : ', err);
                console.log('Installer ready!', releasesDir.path(finalPackageName));
                done();
            });

        }

        if (platform === 'linux_64' || platform === 'linux_32') {

            //tmpDir = projectDir.dir(path.resolve('./tmp/'+platform), {  empty:false});
            tmpDir = projectDir.dir(path.resolve('./tmp' + platform), { empty: false });

            var packName = manifest.name;// + '_' + manifest.version;

            var debFileName = packName + '_' + platform + '.deb';
            var debPath = releasesDir.path(debFileName);

            var packDir = tmpDir.dir(packName);

            console.log('run grunt installer ' + platform);

            var readyAppDir = packDir.cwd('opt', manifest.name);

            console.log('Creating DEB package...');

            // Counting size of the app in KiB
            console.log('inspect tree : ', readyAppDir.path());
            var appSize = Math.round(readyAppDir.inspectTree('.').size / 1024);

            // Preparing debian control file
            var control = projectDir.read('resources/linux/DEBIAN/control');
            control = utils.replace(control, {
                name: manifest.name,
                description: manifest.description,
                version: manifest.version,
                author: manifest.author,
                size: appSize,
                arch: is64 ? 'amd64' : 'i386'
            });

            packDir.write('DEBIAN/control', control);

            console.log('packDir ' + packDir.path());
            console.log('readyAppDir ' + readyAppDir.path());
            console.log('debPath ' + debPath);


            var postinst = projectDir.read('resources/linux/DEBIAN/postinst');

            packDir.write('DEBIAN/postinst', postinst);


            child_process.exec('chmod 755 ' + packDir.path() + '/DEBIAN/postinst');

            // Build the package...
            child_process.exec('fakeroot dpkg-deb -Zxz --build ' + packDir.path().replace(/\s/g, '\\ ') + ' ' + debPath.replace(/\s/g, '\\ '),
                function (error, stdout, stderr) {
                    if (error || stderr) {
                        console.log("ERROR while building DEB package:");
                        console.log(error);
                        console.log(stderr);
                    } else {
                        console.log('DEB package ready!', debPath);
                    }
                    deferred.resolve();
                });
        }

    });

    grunt.registerTask('platform', 'platform independent electron task. \nThis compiles a new electron app, and copies all ' +
        'files over to tmp[OS] and if --dist==true it copies into the final dist directory', function () {

            grunt.task.run('shell:electron');

            if (OS == 'osx') {
                //let the linux host copy files over via ssh
                grunt.task.run('sshexec:cp-dist-all-osx');
            }

            if (platform == 'win32') {
                //let the linux host copy files over via ssh
                grunt.task.run('sshexec:cp-dist-all-windows');
            }

            if (OS == 'linux_32') {
                //let the linux host copy files over via ssh
                grunt.task.run('sshexec:cp-dist-all-linux_32');
            }

            grunt.task.run('copy-platform-servers');
            grunt.task.run('copy:dist-misc');

            grunt.task.run('chmod:xcf-server');
            //grunt.task.run('chmod:php');
            //grunt.task.run('chmod:nginx');
            //grunt.task.run('chmod:nginx-misc');
            grunt.task.run('chmod:mongo');
            grunt.task.run('chmod:dist');
            grunt.task.run('chmod:scripts');
            if (OS == 'osx') {
                //let the linux host copy files over via ssh
                //grunt.task.run('sshexec:cp-dist-all-osx');
            }

            if (OS == 'linux_32' || OS == 'linux_64') {
                grunt.task.run('chmod:xide');
            }
            if (grunt.option('dist') === true) {
                grunt.task.run('dist-final');
            }
        });
    //////////////////////////////////////////////////////////////
    //
    //  Update tasks, applied in existing build directory (./tmp[Platform])
    //

    //build & update only electron in temp[OS] with gulp release --env=production --clean=false
    grunt.registerTask('electron-update', [
        'shell:electron-update'
    ]);
};
