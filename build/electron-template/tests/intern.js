// jshint unused: false
//var dojoConfig = { async: true };
define({
	// The port on which the instrumenting proxy will listen
	proxyPort: 9000,
	// A fully qualified URL to the Intern proxy
	proxyUrl: 'http://localhost:9000/',

	// Username and key should be set in the BROWSERSTACK_USERNAME and BROWSERSTACK_ACCESS_KEY environment variables
	tunnel: 'NullTunnel',

	// See https://www.browserstack.com/automate/capabilities for capabilities and environments
	capabilities: {
		name: 'dojo-loader',
		project: 'Dojo 2',
		fixSessionCapabilities: false
	},

	environments: [
		// Enter whichever browser you want to test here.
		// (It is unwise to test more than one simultaneously on one host,
		// due to potential for spurious focus test failures.)
		{ browserName: 'internet explorer', version: ['9.0', '10.0', '11.0'], platform: 'Windows 7' } /*,
		 { browserName: 'microsoftedge', platform: 'Windows 10' }*/,
		{ browserName: 'firefox', platform: 'Windows 10' },
		{ browserName: 'chrome', platform: 'Windows 10' },
		{ browserName: 'safari', version: '9', platform: 'OS X 10.11' },
		{ browserName: 'android', platform: 'Linux', version: '4.4', deviceName: 'Google Nexus 7 HD Emulator' }
		/*,{ browserName: 'safari', version: '7', platform: 'OS X 10.9' }*/
	],

	// Maximum number of simultaneous integration tests that should be executed on the remote WebDriver service
	maxConcurrency: 2,

	/*basePath: '..',*/

	// Configuration options for the module loader;
	// any AMD configuration options supported by the Dojo loader can be used here
			/*
	loaders: {
		//'host-browser': 'dojo/dojo.js'
	},
	*/
	loaderOptions: {
		// Packages that should be registered with the loader in each testing environment
		packages: [
			{ name: 'dojo', location: './node_modules/dojo' },
			{ name: 'dijit', location: 'dijit' },
			{ name: 'dgrid', location: 'dgrid' },
			{ name: 'dstore', location: 'dstore' },
			{ name: 'xide', location: 'xide' },
			{ name: 'xfile', location: 'xfile' },
			{ name: 'dojox', location: 'dojox' },
			{ name: 'xdojo', location: 'xdojo' },
			{ name: 'xgrid', location: 'xgrid' },
			{ name: 'xblox', location: 'xblox' },
			{ name: 'xdocker', location: 'xdocker' },
			{ name: 'wcDocker', location: 'wcDocker/Code' },
			{ name: 'dcl', location: './node_modules/dcl' },
			{ name: 'tests', location: 'tests' }
		]
	},

	// A regular expression matching URLs to files that should not be included in code coverage analysis
	//excludeInstrumentation: /^dojox?|^dijit|^dstore|\/node_modules\/|\/tests\/|\/nls\//,
	excludeInstrumentation : /(?:node_modules|bower_components|tests)[\/\\]/,

	// Non-functional test suite(s) to run in each browser
	suites: [ 'tests/all' ],

	// Functional test suite(s) to run in each browser once non-functional tests are completed
	functionalSuites: [ 'tests/intern/functional' ]
});
