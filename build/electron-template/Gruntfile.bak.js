/*global module */
module.exports = function (grunt) {


    function log(err, stdout, stderr, cb) {
        console.log(arguments);
        cb();
    }

    var path = require('path');
    var os = require('os');
    var os_suffix='Linux';

    if(os.platform() ==='win32'){
        os_suffix = 'Windows';
    }else if(os.platform() ==='darwin'){
        os_suffix = 'OSX';
    }

    var DIST_DIRECTORY = path.resolve('./tmp' + os_suffix +'/Control-Freak');

    var ROOT = path.resolve('../../');
    var SERVER_TEMPLATE = path.resolve('./../www-server-template');
    var BUILD_ROOT = path.resolve('../');


    var DIST_LINUX_MAIN =  path.resolve('./tmp' + os_suffix +'/Control-Freak_1.0.0/opt/Control-Freak');

    var DEVICE_SERVER = path.resolve('../../Code/utils/dist/'+os_suffix.toLowerCase());

    //console.error('DEVICE_SERVER ' +DEVICE_SERVER);


    var extraArgsGulp = process.argv.slice(3).join(' ');

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON("package.json"),
        shell: {
            electron: {
                command: [
                    'gulp release --env=production ' +extraArgsGulp
                ].join('&&'),
                options: {
                    stderr: true

                }
            },
            "electron-update": {
                command: [
                    'gulp release --env=production --clean=false ' +extraArgsGulp
                ].join('&&'),
                options: {
                    stderr: true
                }
            }
        },
        copy:{
            "dist-windows-nginx":{
                src:[
                    '**'
                ],
                dest:DIST_DIRECTORY,
                expand: true,
                flatten: false,
                cwd:SERVER_TEMPLATE + '/nginx'
            },
            "dist-windows-php":{
                src:[
                    '**'
                ],
                dest:DIST_DIRECTORY + '/php',
                expand: true,
                flatten: false,
                cwd:SERVER_TEMPLATE + '/php'
            },
            "dist-windows-utils":{
                src:[
                    '**'
                ],
                dest:DIST_DIRECTORY,
                expand: true,
                flatten: false,
                cwd:SERVER_TEMPLATE + '/utils'
            },
            "dist-windows-temp":{
                src:[
                    '**'
                ],
                dest:DIST_DIRECTORY +'/temp',
                expand: true,
                flatten: false,
                cwd:SERVER_TEMPLATE + '/temp'
            },
            "dist-windows-mongo":{
                src:[
                    '**'
                ],
                dest:DIST_DIRECTORY +'/mongo',
                expand: true,
                flatten: false,
                cwd:SERVER_TEMPLATE + '/mongo'
            },
            "dist-windows-code":{
                src:[
                    '**'
                ],
                dest:DIST_DIRECTORY,
                expand: true,
                flatten: false,
                cwd:ROOT + '/dist/all'
            },
            /////////////////////////////////////////
            //
            //  Linux
            //
            "dist-linux-code":{
                src:[
                    '**'
                ],
                dest:DIST_LINUX_MAIN,
                expand: true,
                flatten: false,
                cwd:ROOT + '/dist/all'
            },
            "dist-linux-nginx":{
                src:[
                    '**',
                    '!nginx-1.9.13/**',
                    '!nginx-1.9.13.tar',
                    '!error.log',
                    '!access.log',
                    '!docs/**',
                    '!contrib/**',
                    '!nginx-armv7l',
                    '!build.sh',
                    '!download.sh'

                ],
                dest:DIST_LINUX_MAIN,
                expand: true,
                flatten: false,
                cwd:SERVER_TEMPLATE + '/nginx-build/'
            },
            "dist-linux-server":{
                src:[
                    '**'
                ],
                dest:DIST_LINUX_MAIN +'/Code/utils/app/xide',
                expand: true,
                flatten: false,
                cwd:DEVICE_SERVER
            }
        },
        clean:{
            dist:['dist/all/Code/**','dist/all/data/**']
        }

    });


    grunt.loadNpmTasks('grunt-shell');
    grunt.loadNpmTasks("grunt-contrib-copy");
    grunt.loadNpmTasks('grunt-contrib-clean');


    grunt.registerTask('dist-windows',[
        'copy:dist-windows-nginx',
        'copy:dist-windows-php',
        'copy:dist-windows-mongo',
        'copy:dist-windows-utils',
        'copy:dist-windows-temp',
        'copy:dist-windows-code'
    ]);

    grunt.registerTask('dist-linux',[
        "copy:dist-linux-code",
        "copy:dist-linux-nginx",
        "copy:dist-linux-server"        //server-linux.exe from Code/utils/dist/linux
        //'copy:dist-linux-utils'
        /*'copy:dist-windows-temp',
        'copy:dist-windows-code'*/
    ]);


    grunt.registerTask('windows', [
        'shell:electron',
        'dist-windows'
    ]);

    grunt.registerTask('linux', [
        'shell:electron',
        'dist-linux'
    ]);

    // Aliases
    grunt.registerTask('dist-prepare', [
        'clean:dist',
        'copy:dist-xapp',
        'dist-client',
        'dist-data',
        'dist-utils'
    ]);

    //////////////////////////////////////////////////////////////
    //
    //  Update tasks, applied in existing build directory (./tmp[Platform])
    //

    //build & update only electron in temp[OS] with gulp release --env=production --clean=false
    grunt.registerTask('electron-update', [
        'shell:electron-update'
    ]);
};