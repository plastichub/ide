var Q = require('q');
var pathUtil = require('path');
var path = require('path');
var isWin32 = process.platform === 'win32';
var requireg=require('requireg');
var electron = requireg('electron');

var childProcess = require('child_process');
var utils = require('./utils');
var argv = require('yargs').argv;


var gulpPath = pathUtil.resolve('./node_modules/.bin/gulp');
if (process.platform === 'win32') {
    gulpPath += '.cmd';
}

var runBuild = function () {
    var deferred = Q.defer();
    var build = childProcess.spawn(gulpPath, [
        'build',
        '--env=' + utils.getEnvName(),
        '--color'
    ], {
        stdio: 'inherit'
    });

    build.on('close', function (code) {
        deferred.resolve();
    });

    return deferred.promise;
};

var runApp = function () {
    //console.log('start',electron);
    //console.log('start',argv);
    var extra = [];
    for(var arg in argv){
        if(arg==='_' ||arg==='$0'){
            continue;
        }
        //extra.push(arg);
        //extra.push(argv[arg]);
        //var _arg = {}
        //_arg[arg] = argv[arg];
        //extra.push(_arg);
    }

    //console.log('start',['./build'].concat(extra));
    var app = childProcess.spawn(electron, ['./build'].concat(extra), {
        stdio: 'inherit'
    });

    app.on('close', function (code) {
        // User closed the app. Kill the host process.
        /*
        kill(watch.pid, 'SIGKILL', function () {
            process.exit();
        });*/
    });
};
runBuild()
.then(function () {
    //runGulpWatch();
    runApp();
});
