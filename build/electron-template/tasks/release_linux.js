'use strict';

var Q = require('q');
var gulpUtil = require('gulp-util');
var childProcess = require('child_process');
var jetpack = require('fs-jetpack');
var asar = require('asar');
var os = require('os');
var utils = require('./utils');
var argv = require('yargs').argv;

var path = require('path');
var isWin32 = process.platform === 'win32';
var globalPrefix = isWin32 ? ( process.env.APPDATA + '/npm/') : '/usr/local/lib/';
var globalE = path.resolve(globalPrefix + '/node_modules/electron');
var projectDir;
var releasesDir;
var packName;
var packDir;
var readyAppDir;
var manifest;
var serverDir;
var debug = true;
var platform = os.platform();
var arch = os.arch();
var tmpDir = "./tmplinux";
if (arch!=='x64') {
    tmpDir += "_32";
} else if (arch==='x64') {
    tmpDir += "_64";
}

//var args =process.argv.slice(3).join(' ');
var packageToDeb= argv.package ==='true';

console.error('package : ' + packageToDeb);

var init = function () {
    projectDir = jetpack;
    tmpDir = projectDir.dir(tmpDir, { empty: packageToDeb!==true });
    releasesDir = projectDir.dir('./releasesLinux');
    manifest = projectDir.read('app/package.json', 'json');
    packName = manifest.name;// + '_' + manifest.version;
    packDir = tmpDir.dir(packName);
    readyAppDir = packDir.cwd('opt', manifest.name);
    serverDir = projectDir.dir('../www-server-template');

    return Q();
};

var copyRuntime = function () {
    console.error('-'+globalE + '/dist');
    return projectDir.copyAsync(globalE + '/dist', readyAppDir.path(), { overwrite: true });
};

var packageBuiltApp = function () {
    var deferred = Q.defer();

    asar.createPackage(projectDir.path('build'), readyAppDir.path('resources/app.asar'), function () {
        deferred.resolve();
    });

    return deferred.promise;
};

var finalize = function () {

    // Create .desktop file from the template
    var desktop = projectDir.read('resources/linux/app.desktop');
    desktop = utils.replace(desktop, {
        name: manifest.name,
        productName: manifest.productName,
        description: manifest.description,
        version: manifest.version,
        author: manifest.author
    });
    packDir.write('usr/share/applications/' + manifest.name + '.desktop', desktop);

    var server = projectDir.read('resources/linux/server.desktop');
    server = utils.replace(server, {
        name: manifest.name,
        productName: manifest.productName,
        description: manifest.description,
        version: manifest.version,
        author: manifest.author
    });
    packDir.write('usr/share/applications/' + manifest.name + '-server.desktop', server);

    // Copy icon
    projectDir.copy('resources/icon.png', readyAppDir.path('icon.png'));

    return Q();
};

var renameApp = function () {
    return readyAppDir.renameAsync("electron", manifest.name);
};


var packToDebFile = function () {
    var deferred = Q.defer();

    var debFileName = packName + '_amd64.deb';
    var debPath = releasesDir.path(debFileName);

    gulpUtil.log('Creating DEB package...');

    // Counting size of the app in KiB
    var appSize = Math.round(readyAppDir.inspectTree('.').size / 1024);

    // Preparing debian control file
    var control = projectDir.read('resources/linux/DEBIAN/control');
    control = utils.replace(control, {
        name: manifest.name,
        description: manifest.description,
        version: manifest.version,
        author: manifest.author,
        size: appSize
    });
    packDir.write('DEBIAN/control', control);

    var postinst = projectDir.read('resources/linux/DEBIAN/postinst');

    packDir.write('DEBIAN/postinst', postinst);

    childProcess.exec('chmod 755 ' + packDir.path() +'/DEBIAN/postinst');

    // Build the package...
    childProcess.exec('fakeroot dpkg-deb -Zxz --build ' + packDir.path().replace(/\s/g, '\\ ') + ' ' + debPath.replace(/\s/g, '\\ '),
        function (error, stdout, stderr) {
            if (error || stderr) {
                console.log("ERROR while building DEB package:");
                console.log(error);
                console.log(stderr);
            } else {
                gulpUtil.log('DEB package ready!', debPath);
            }
            deferred.resolve();
        });

    return deferred.promise;
};

var cleanClutter = function () {
    return tmpDir.removeAsync('.');
};

var dummy = function(){
    var deferred = Q.defer();
    deferred.resolve(true);
    return deferred.promise;
}

var installer = packageToDeb ?  packToDebFile : dummy;

console.error('create installer ' + packageToDeb);

module.exports = function () {

    if(packageToDeb){
        return init()
            .then(packToDebFile)
            .catch(function(error){
                console.error('Electron - ERROR: ERROR',error);
            })
    }

    return init()
    .then(copyRuntime)
    .then(packageBuiltApp)
    .then(finalize)
    .then(renameApp)
    //.then(installer)
    //.then(cleanClutter)
    .catch(function(error){
        console.error('Electron - ERROR: ERROR',error);
    });
};
