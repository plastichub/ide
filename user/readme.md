# Touchscreen/remote interface for various PP machines

## Communication

Raspberry-PI connects via serial (USB) to Arduino, also to enable remote firmware updates. The PI or whatever connects to the Arduino will
share the device through other protocols, MQTT,... 

## Protocols

### Serial

Since it's serial, we receive for each command a reply matching an issue id as well a payload with the requested data or command replies.

Response construction via delimitter : 10|x0A - line by line

Send Data/Command Syntax : ID | VERB | LENGTH | PAYLOAD

ID : queued/issued command id, used to confirm command on sender side.
VERB : COMMAND,STATUS,DATA
LENGTH : Length of the payloay (not really needed, yet)
PAYLOAD: Byte sequence

Reply syntax : ID | STATUS | LENGTH | *PAYLOAD*

ID: queued/issued command id, used to confirm command on sender side
STATUS : Error code, OK=0, SERVERITY Mask (syslog)
LENGTH : Length of the payloay (not really needed, yet)
PAYLOAD: Byte sequence
