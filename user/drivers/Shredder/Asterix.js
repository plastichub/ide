/** @module xcf/driver/DefaultDriver */
define([
    "dcl/dcl",
    'xdojo/has',
    'xide/utils'
], function (dcl, has, utils) {

    // The returning module
    var Module = null;

    //////////////////////////////////////////////////////////
    //
    //  Constants
    //
    var isServer = has('host-node');        // We are running server-side ?
    var isIDE = has('xcf-ui');              // We are running client-side and in the IDE?

    /**
     * Default driver template. This will used for new drivers!
     *
     * @class module:xcf/driver/DefaultDriver
     * @extends module:xcf/driver/DriverBase
     * @augments module:xide/mixins/EventedMixin
     * @link http://rawgit.com/net-commander/windows-dist/master/docs/Driver/modules/module-xcf_driver_DriverBase.html
     */
    Module = dcl(null, {
        /***
         * Standard callback when we have a message from the device we're bound to (specified in profile).
         * 1. put the message in the incoming queue, tag it as 'unread'
         * 2. in case we have messages to send and we are in 'onReply' mode, trigger outgoing queue
         *
         * @param data {Object} : Message Struct build by the device manager
         * @param data.device {Object} : Device info
         * @param data.device.host {String} : The host
         * @param data.device.port {String} : The host's port
         * @param data.device.protocol {String} : The host's protocol

         * @param data.message {String} : RAW message, untreated
         */
        onMessage: function (data) {
            if (!data.message.length) {
                return;
            }
            let m = data.message.trim();
            if (m.startsWith('<<') && m.endsWith('>>')) {
                let payload = m.substring(2, m.length - 2);
                console.log("on driver message : \n", payload);
                let parts = payload.split(";");
                if (parseInt(parts[0]) === 1000) {
                    try {
                        let data = JSON.parse("[" + parts[2] + "]");
                        console.dir(data);
                        data.forEach(element => {
                            // console.log("var " + element.name, element.state);
                            switch (element.name) {
                                case "VDF": {
                                    this.setVariable("OpMode", element[0]);
                                    break;
                                }
                                case "Power": {
                                    this.setVariable("PrimaryPower", "" + data[0][0], true, true);
                                    this.setVariable("SecondaryPower", "" + data[0][1], true, true);
                                    break;
                                }
                                case "OpMode": {
                                    this.setVariable("OpMode", element[0]);
                                    break;
                                }
                                case "Plunger": {
                                    this.setVariable("Plunger", element.state, true);
                                    this.setVariable("PlungerFlags", element.flags, true);
                                    break;
                                }
                                case "HopperLoaded": {
                                    this.setVariable("HopperLoaded", element.state);
                                    break;
                                }
                            }
                        });
                    } catch (e) {
                        console.error("error data", e);
                    }
                }

            } else {
                console.log(m);
            }

        }
    });


    //////////////////////////////////////////////////////////
    //
    //  Optional: An example implementation to extend commands in the interface for additional fields
    //
    if (isIDE) {
        /**
         *
         * @param command {module:xcf/model/Command} The command which for which want to populate the fields.
         * @param fields {Object[]}
         * @link http://rawgit.com/net-commander/windows-dist/master/docs/Driver/modules/xcf_model_Command.js.html
         */
        Module.getFields = function (command, fields) {
            /*
            return [utils.createCI('test', 0, command.test, {
                group: 'General',
                title: 'test',
                dst: 'test',
                order: 198
            })];
            */
            return [];
        };
    }

    //////////////////////////////////////////////////////////
    //
    //  Optional: An example implementation to modify the string sent to a advice. This makes sense if you added additional fields as shown above.  
    //
    /**
     * Callback when a command is before parsing the expression in the "send" field. 
     * @param command {module:xcf/model/Command}
     * @param inputString {string}
     * @returns {string}
     */
    /*
    Module.resolveBefore = function (command,inputString) {
        return inputString;
    };
    */
    /**
     * Callback when a command was parsing the expression in the "send" field.
     * @param command {module:xcf/model/Command}
     * @param inputString {string}
     * @returns {string}
     */
    /*
    Module.resolveAfter = function (command,inputString) {
        return inputString;
    };
    */

    return Module;
});

