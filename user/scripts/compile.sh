AVR=/home/pi/arduino-1.8.12
AHOME=/home/pi/.arduino15
mkdir /tmp/arduino_cache

PROJECT=/home/pi/firmware/shredder-extrusion/firmware-next/firmware-next.ino

$AVR/arduino-builder -compile -logger=machine -hardware $AVR/hardware -hardware $AHOME/packages -tools $AVR/tools-builder -tools $AVR/hardware/tools/avr -tools $AHOME/packages -built-in-libraries $AVR/libraries -libraries /home/pi/Arduino/libraries -fqbn=CONTROLLINO_Boards:avr:controllino_mini -vid-pid=0X2341_0X0043 -ide-version=10812 -build-path /tmp/arduino -warnings=none -build-cache /tmp/arduino_cache -prefs=build.warn_data_percentage=75 -prefs=runtime.tools.avrdude.path=$AVR/hardware/tools/avr -prefs=runtime.tools.avrdude-6.3.0-arduino8.path=$AHOME/packages/arduino/tools/avrdude/6.3.0-arduino8 -prefs=runtime.tools.avr-gcc.path=$AVR/hardware/tools/avr -prefs=runtime.tools.avr-gcc-4.9.2-atmel3.5.3-arduino2.path=$AHOME/packages/arduino/tools/avr-gcc/4.9.2-atmel3.5.3-arduino2 -prefs=runtime.tools.arduinoOTA.path=$AVR/hardware/tools/avr -prefs=runtime.tools.arduinoOTA-1.3.0.path=$AVR/hardware/tools/avr -prefs=runtime.tools.avr-gcc-7.3.0-atmel3.6.1-arduino5.path=$AVR/hardware/tools/avr -prefs=runtime.tools.avrdude-6.3.0-arduino17.path=$AVR/hardware/tools/avr $PROJECT

